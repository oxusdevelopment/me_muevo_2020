<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuestionUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuestionUsersTable Test Case
 */
class QuestionUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\QuestionUsersTable
     */
    public $QuestionUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.QuestionUsers',
        'app.DisabilityScales',
        'app.ActivityLimitations',
        'app.SelfRatedHealth',
        'app.CommunicationPhysicians',
        'app.ArthritisSelftEfficacies',
        'app.Users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('QuestionUsers') ? [] : ['className' => QuestionUsersTable::class];
        $this->QuestionUsers = TableRegistry::getTableLocator()->get('QuestionUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuestionUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
