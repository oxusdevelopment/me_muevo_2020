<!DOCTYPE html>
<html>

<head>
	<style type="text/css">
		.mail {
			width: 80%;
			margin: auto;
			border: 0.5px solid #E0E0E0;
		}

		header {
			height: 100px;
			background-color: #E0E0E0;
			width: 100%;
			display: flex;
			position: relative;
			justify-content: center;
		}

		.logo {
			width: 116px;
			height: 52px;
			margin: auto;
		}

		.content {
			height: auto;
			width: 100%;
			color: #333333;
		}

		.title {
			text-align: center;
			font-size: 25px;
		}

		.content p {
			text-align: center;
			font-size: 15px;
		}

		.link-box {
			height: 80px;
			width: 80%;
			margin: 30px auto;
			position: relative;
			display: flex;
		}

		.link {
			text-align: center;
			font-weight: bold;
			font-size: 15px;
			display: block;
			margin: auto;
			text-decoration: none;
		}

		footer {
			width: 100%;
			height: 88px;
		}

		footer p {
			text-align: center;
			font-size: 13px;
			color: #333333;
			font-weight: bold;
		}

		.multipleTariffs {
			width: 80%;
			margin: auto;
			border-collapse: collapse;
			border-spacing: 1px;
			text-align: center;
		}

		.multipleTariffs td,
		th {
			border: 1px solid #f4f4f4;
			padding: 10px;
		}

		.multipleTariffs thead tr th {
			background: #dff0d8;
		}
	</style>
</head>

<body>
	<div class="mail">
		<header>
			<img class="logo" src="http://divorcio.oxus.cl/img/logo.png">
		</header>
		<div class="content">
			<h2 class="title">Estimado Usuario </h2>
			<p>Se le comunica que hay un cliente que quiere contactarse con ud, los datos son:</p>
			<center><?php foreach ($valores as $file) : ?>
				<?php echo $file->field_name.": ".$file->value; ?>
				<br>
			<?php endforeach; ?></center>
		</div>
	</div>
</body>

</html>