$(document).ready(function()
{
	var animacion_cajas = (function()
	{
		var $servicio = $('.servicio');
		var contador = 0;
		var cantServicios = $servicio.length;

		var activaServicio = function(){
			if( contador >= cantServicios )
			{
				contador = 0;
			}
			$servicio.removeClass('servicio-activo');
			$servicio.eq(contador).addClass('servicio-activo');
			contador += 1;
		}
		var servicioInit = function(){
			servicioInterval = setInterval(activaServicio,4000);
		}
		activaServicio();
		servicioInit();
	})();

		var is_open = 0;
		var $menu = $('.smenu');
		var $boton= $('.boton-menu');
		var $ventana = $(window);
		var $cuerpo = $('.cuerpo');
		var altoCabecera = $cuerpo.offset().top;

		$ventana.resize(redimensionaVentana);

		function redimensionaVentana()
		{
			var anchoVentana = window.innerWidth;
			if( anchoVentana < 800 )
			{
				$menu.css({'right':'-100%'});
			}
			if (anchoVentana > 800)
			{
				$menu.css({'right':'0%'});
				$menu.css({'width':'100%'});

				//$menu.css({'display':'inline'});
			}
			is_open = 0;
		}

		$('div.boton-menu').click(menu_responsive);

		function menu_responsive(event)
		{
			event.preventDefault();
			if( is_open == 1 )
			{
				$menu.css({'display':'inline-block'})
				close_nav();
			}
			else
			{

				open_nav();

			}
		}

		var open_nav = function()
		{

			$menu.animate({'right':'0'});
			$menu.css({'width':'180px'});
			$menu.css({'padding-top':'50px'});
			is_open = 1;
		}

		var close_nav = function()
		{
			$menu.css({'width':'200px'});
			$menu.animate({'right':'-100%'});
			is_open = 0;
		}


		var slider = (function(){
			var totalSlides = $('.slide').length;
			var $listaSlides = $('.lista-slider');
			var actualSlide = 0;
			var proximaSlide = 1;
			var sliderInterval;

			$("<div class='paginacion-slider'></div>").appendTo('.contenedor-slider');
			for( i=1;i <= totalSlides;i++ )
			{
				$("<span class='item-paginacion'></span>").appendTo('.paginacion-slider');
			}

			var $itemPaginacion = $('.item-paginacion');
			$itemPaginacion.eq(0).addClass("item-paginacion-activo");

			var movSlider = function(){
				if( proximaSlide >= totalSlides )
				{
					proximaSlide = 0;
				}

				$itemPaginacion.removeClass("item-paginacion-activo").eq(proximaSlide).addClass("item-paginacion-activo");
				$listaSlides.animate({'left':(-100 * proximaSlide)+'%'},1000);

				actualSlide = proximaSlide;
				proximaSlide += 1;
			}

			var sliderInit = function(){
				sliderInterval = setInterval(movSlider,6000);
			}
			sliderInit();

			$itemPaginacion.click(function(){
				var slideDestino = $(this).index();
				if( slideDestino != actualSlide ){
					proximaSlide = slideDestino;
					movSlider();
					clearInterval(sliderInterval);
					sliderInit();
				}
			});

		})();

		function changeClass()
		{
		 document.getElementById("open-hide").classList.toggle('show');
		}

		window.onload = function()
		{
		  document.getElementById("open-hide").addEventListener( 'click' , changeClass );
		}

		

});
