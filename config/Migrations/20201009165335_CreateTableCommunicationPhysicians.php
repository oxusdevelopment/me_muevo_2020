<?php
use Migrations\AbstractMigration;

class CreateTableCommunicationPhysicians extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('communication_physicians');

        $table->addColumn('medial_question', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('make_question', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('personal_problem', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
