<?php
use Migrations\AbstractMigration;

class AlterTableuserAddColumnDocument extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');

        $table->addColumn('document','integer',[
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }
}
