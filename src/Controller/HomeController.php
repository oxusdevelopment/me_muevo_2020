<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use \Datetime;
use Intervention\Image\ImageManagerStatic as Image;

class HomeController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Contenidos');
    }

    public function index()
    {

        $this->paginate = ['limit' => 10];
        $query = $this->Contenidos
            ->find('all')
            ->where(['Categoria_Contenido' => '8',])
            ->order(['Titulo' => 'ASC']);
        $files = $this->paginate($query);
        $this->set(compact('files'));
    }

    public function editEntrada($ID_Contenido)
    {
        $entrada = $this->Contenidos
            ->find('all')
            ->where(['ID_Contenido' => $ID_Contenido])
            ->order(['ID_Contenido' => 'ASC'])->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Contenidos->patchEntity($entrada, $this->request->getData());
            $editDate = date('y-m-d');
            $entrada->Modified = $editDate;
            if ($this->Contenidos->save($entrada)) {
                $this->Flash->success(__('La entrada ha sido actualizada correctamente.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('No ha sido posible actualizar la entrada, por favor reintente.'));
            }
        }

        $this->set('entrada', $entrada);
    }




    // public function addDocument()
    // {
    //     $documento = $this->Contenidos->createDocumento(8);
    //     // $documento->autor___con = $this->getCurrentUser()['usernameusu'];

    //     if ($this->request->is('post')) {

    //         $creationDate = new Datetime();
    //         $documento = $this->Contenidos->patchEntity($documento, $this->request->getData(), [
    //             'validate' => 'default'
    //         ]);
    //         $documento->Created = $creationDate;
    //         if ($this->Contenidos->save($documento)) {
    //             $this->Flash->success(__("Documento ha sido creado correctamente."));
    //             return $this->redirect(['action' => '/index']);
    //         }
    //         //print_r($documento); die();
    //         $this->Flash->error(__("No ha sido posible crear documento, por favor compruebe los datos ingresados."));
    //     }

    //     $this->set(compact('documento'));
    // }


    public function add()
    {
        $contenido = $this->Contenidos->newEntity();

        if ($this->request->is('post')) {
           
            $contenido = $this->Contenidos->patchEntity($contenido, $this->request->getData());
            $creationDate = date('y-m-d');
            $contenido->Created = $creationDate;
            $contenido->Categoria_Contenido ='8';
            $contenido->Modified = NULL;        
            if ($this->Contenidos->save($contenido)) {
                
                $this->Flash->success(__('El contenido ha sido creado correctamente.'));
                return $this->redirect(['action' => '/index']);
            }
            $this->Flash->error(__('No ha sido posible crear el contenido, por favor compruebe los datos ingresados.'));
        }
        $this->set(compact('contenido'));
    }

    public function edithome()
    {
        $this->paginate = ['limit' => 10];
        $query = $this->Contenidos
            ->find('all')
            ->where(['Categoria_Contenido' => '1',])
            ->order(['Titulo' => 'ASC']);
        $files = $this->paginate($query);
        $this->set(compact('files'));
    }

    public function editContenido ($ID_Contenido)
    {
        $entrada = $this->Contenidos
            ->find('all')
            ->where(['ID_Contenido' => $ID_Contenido])
            ->order(['ID_Contenido' => 'ASC'])->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->Contenidos->patchEntity($entrada, $this->request->getData());
            $editDate = date('y-m-d');
            $entrada->Modified = $editDate;
            if ($this->Contenidos->save($entrada)) {
                $this->Flash->success(__('La entrada ha sido actualizada correctamente.'));
                return $this->redirect(['action' => '/index']);
            } else {
                $this->Flash->error(__('No ha sido posible actualizar la entrada, por favor reintente.'));
            }
        }

        $this->set('entrada', $entrada);
    }

    public function delete($ID_Contenido =null)
    {  
        $slider = $this->Contenidos->get($ID_Contenido);
        if ($this->Contenidos->delete($slider)) {
            $this->Flash->success(__('Contenido eliminado'));
        } else {
            $this->Flash->error(__('El contenido no pudo ser eliminado, intente nuevamente.'));
        }        

        return $this->redirect(['action' => '/index']);
    }
}
