<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;
/**
 * User Entity
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property string $lastname
 * @property int $group_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string|null $rut
 * @property string|null $birth_date
 * @property int|null $sex
 * @property int|null $healt
 * @property int|null $phone
 * @property string|null $detection
 *
 * @property \App\Model\Entity\Group $group
 * @property \App\Model\Entity\ActivityLimitation[] $activity_limitations
 * @property \App\Model\Entity\ArthritisSelftEfficacy[] $arthritis_selft_efficacies
 * @property \App\Model\Entity\CommunicationPhysician[] $communication_physicians
 * @property \App\Model\Entity\DisabilityScale[] $disability_scales
 * @property \App\Model\Entity\QuestionUser[] $question_users
 * @property \App\Model\Entity\SelfRatedHealth[] $self_rated_health
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'password' => true,
        'name' => true,
        'lastname' => true,
        'group_id' => true,
        'created' => true,
        'modified' => true,
        'rut' => true,
        'birth_date' => true,
        'sex' => true,
        'healt' => true,
        'phone' => true,
        'detection' => true,
        'group' => true,
        'activity_limitations' => true,
        'arthritis_selft_efficacies' => true,
        'communication_physicians' => true,
        'disability_scales' => true,
        'question_users' => true,
        'self_rated_health' => true,
        'document' => true,

    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password'
    ];
    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
