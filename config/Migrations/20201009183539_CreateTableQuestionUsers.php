<?php
use Migrations\AbstractMigration;

class CreateTableQuestionUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('question_users');

        $table->addColumn('disability_scale_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('activity_limitation_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('self_rated_health_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);


        $table->addColumn('communication_physician_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('arthritis_selft_efficacy_id', 'integer', [
            'default' => null,
            'null' => true,
        ]);

        $table->addColumn('user_id', 'integer', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
