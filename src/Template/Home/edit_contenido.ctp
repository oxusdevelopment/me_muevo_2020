<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Home
        <small>Preview page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?= $this->Form->create($entrada, ['type' => 'file']); ?>
        <div class="col-md-3 col-md-push-9 special-box">
            <div class="box box-purple">
                <div class="box-header with-border">
                    <h3 class="box-title">Navegación</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-toolbar">
                                <?= $this->Form->button(__('<i class="fa fa-floppy-o fa-side"></i> Guardar'), ['class' => 'btn btn-default option']) ?>
                            </div>
                            <div class="btn-toolbar">
                                <?= $this->Html->link(__('<i class="fa fa-repeat fa-side"></i> Volver'), ['action' => '/index'], ['class' => 'btn btn-default option', 'escape' => false]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-md-pull-3 special-box">
            <div class="box box-purple">
                <div class="box-header with-border">
                    <h3 class="box-title">Home</h3>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <?= $this->Form->control('Titulo', [
                                'label' => 'Título',
                                'class' => 'form-control',

                            ]);
                            ?>
                        </div>

                        <div class="form-group col-md-12">
                            <?= $this->Form->control('Cuerpo', [
                                'type' => 'textarea',
                                'label' => 'Cuerpo',
                                'class' => 'form-control ckeditor',

                            ]);
                            ?>
                        </div>

                        <div class="form-group col-md-12">
                            <?= $this->Form->control('Enlace_URL', [   
                                'label' => 'URL',
                                'class' => 'form-control ckeditor',

                            ]);
                            ?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</section>