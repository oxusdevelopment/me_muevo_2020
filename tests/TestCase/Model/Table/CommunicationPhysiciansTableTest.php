<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CommunicationPhysiciansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CommunicationPhysiciansTable Test Case
 */
class CommunicationPhysiciansTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\CommunicationPhysiciansTable
     */
    public $CommunicationPhysicians;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CommunicationPhysicians'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('CommunicationPhysicians') ? [] : ['className' => CommunicationPhysiciansTable::class];
        $this->CommunicationPhysicians = TableRegistry::getTableLocator()->get('CommunicationPhysicians', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CommunicationPhysicians);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
