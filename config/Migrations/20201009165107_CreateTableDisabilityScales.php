<?php
use Migrations\AbstractMigration;

class CreateTableDisabilityScales extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('disability_scales');

        $table->addColumn('dress', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('get_up', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('lifting_cup', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('walking', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('swim', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('lifting_clothing', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('open_tap', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('get_up_car', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
