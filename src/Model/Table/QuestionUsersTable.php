<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * QuestionUsers Model
 *
 * @property \App\Model\Table\DisabilityScalesTable|\Cake\ORM\Association\BelongsTo $DisabilityScales
 * @property \App\Model\Table\ActivityLimitationsTable|\Cake\ORM\Association\BelongsTo $ActivityLimitations
 * @property \App\Model\Table\SelfRatedHealthTable|\Cake\ORM\Association\BelongsTo $SelfRatedHealth
 * @property \App\Model\Table\CommunicationPhysiciansTable|\Cake\ORM\Association\BelongsTo $CommunicationPhysicians
 * @property \App\Model\Table\ArthritisSelftEfficaciesTable|\Cake\ORM\Association\BelongsTo $ArthritisSelftEfficacies
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\QuestionUser get($primaryKey, $options = [])
 * @method \App\Model\Entity\QuestionUser newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\QuestionUser[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\QuestionUser|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuestionUser saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\QuestionUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\QuestionUser[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\QuestionUser findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class QuestionUsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('question_users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('DisabilityScales', [
            'foreignKey' => 'disability_scale_id'
        ]);
        $this->belongsTo('ActivityLimitations', [
            'foreignKey' => 'activity_limitation_id'
        ]);
        $this->belongsTo('SelfRatedHealth', [
            'foreignKey' => 'self_rated_health_id'
        ]);
        $this->belongsTo('CommunicationPhysicians', [
            'foreignKey' => 'communication_physician_id'
        ]);
        $this->belongsTo('ArthritisSelftEfficacies', [
            'foreignKey' => 'arthritis_selft_efficacy_id'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['disability_scale_id'], 'DisabilityScales'));
        $rules->add($rules->existsIn(['activity_limitation_id'], 'ActivityLimitations'));
        $rules->add($rules->existsIn(['self_rated_health_id'], 'SelfRatedHealth'));
        $rules->add($rules->existsIn(['communication_physician_id'], 'CommunicationPhysicians'));
        $rules->add($rules->existsIn(['arthritis_selft_efficacy_id'], 'ArthritisSelftEfficacies'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
