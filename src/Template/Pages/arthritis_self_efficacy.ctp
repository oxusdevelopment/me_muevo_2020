<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="title">Percepcion de autoeficacia</h3>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10 col-md-push-1" >
                    <?= $this->Form->create(); ?>
                    <table class="table table-header-rotated" style="font-size: 10px;">
                                <thead>
                                    <tr class="success">
                                        <th></th>
                                        <th class="rotate"><div><strong>Muy inseguro(a)</strong></div></th>
                                        <th class="rotate"><div><strong>1</strong></div></th>
                                        <th class="rotate"><div><strong>2</strong></div></th>
                                        <th class="rotate"><div><strong>3</strong></div></th>
                                        <th class="rotate"><div><strong>4</strong></div></th>
                                        <th class="rotate"><div><strong>5</strong></div></th>
                                        <th class="rotate"><div><strong>6</strong></div></th>
                                        <th class="rotate"><div><strong>7</strong></div></th>
                                        <th class="rotate"><div><strong>8</strong></div></th>
                                        <th class="rotate"><div><strong>9</strong></div></th>
                                        <th class="rotate"><div><strong>10</strong></div></th>
                                        <th class="rotate"><div><strong>Muy seguro(a)</strong></div></th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <p><strong>En las siguientes preguntas nos gustaría saber cómo le afecta el dolor de artritis y qué piensa Ud. de sus habilidades para controlar su artritis. En cada una de las siguientes escalas, por favor marque el número que mejor corresponda a su nivel de seguridad de que puede realizar en este momento las siguientes tareas.</strong></p>
                                    <tr>
                                        <th class="row-header"><strong>1.- ¿Qué tan seguro se siente Ud. de poder reducir bastante su dolor?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name='reduce_pain' required></td>
                                        <td><input type="radio" value='2' name='reduce_pain' required></td>
                                        <td><input type="radio" value='3' name='reduce_pain' required></td>
                                        <td><input type="radio" value='4' name='reduce_pain' required></td>
                                        <td><input type="radio" value='5' name='reduce_pain' required></td>
                                        <td><input type="radio" value='6' name='reduce_pain' required></td>
                                        <td><input type="radio" value='7' name='reduce_pain' required></td>
                                        <td><input type="radio" value='8' name='reduce_pain' required></td>
                                        <td><input type="radio" value='9' name='reduce_pain' required></td>
                                        <td><input type="radio" value='10' name='reduce_pain' required></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>2.- ¿Qué tan seguro se siente Ud. de poder evitar que el dolor de la artritis interfiera con su sueño?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name='sleep_pain' required></td>
                                        <td><input type="radio" value='2' name='sleep_pain' required></td>
                                        <td><input type="radio" value='3' name='sleep_pain' required></td>
                                        <td><input type="radio" value='4' name='sleep_pain' required></td>
                                        <td><input type="radio" value='5' name='sleep_pain' required></td>
                                        <td><input type="radio" value='6' name='sleep_pain' required></td>
                                        <td><input type="radio" value='7' name='sleep_pain' required></td>
                                        <td><input type="radio" value='8' name='sleep_pain' required></td>
                                        <td><input type="radio" value='9' name='sleep_pain' required></td>
                                        <td><input type="radio" value='10' name='sleep_pain' required></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>3.-¿Qué tan seguro se siente Ud. de poder evitar que el dolor de la artritis interfiera con las cosas que quiere hacer?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name='pain_interfere' required></td>
                                        <td><input type="radio" value='2' name='pain_interfere' required></td>
                                        <td><input type="radio" value='3' name='pain_interfere' required></td>
                                        <td><input type="radio" value='4' name='pain_interfere' required></td>
                                        <td><input type="radio" value='5' name='pain_interfere' required></td>
                                        <td><input type="radio" value='6' name='pain_interfere' required></td>
                                        <td><input type="radio" value='7' name='pain_interfere' required></td>
                                        <td><input type="radio" value='8' name='pain_interfere' required></td>
                                        <td><input type="radio" value='9' name='pain_interfere' required></td>
                                        <td><input type="radio" value='10' name='pain_interfere' required></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>4.- ¿Qué tan seguro se siente Ud. de poder regular su actividad para mantenerse activo sin empeorar (agravar) su artritis?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name="fequency_activity" required></td>
                                        <td><input type="radio" value='2' name="fequency_activity" required></td>
                                        <td><input type="radio" value='3' name="fequency_activity" required></td>
                                        <td><input type="radio" value='4' name="fequency_activity" required></td>
                                        <td><input type="radio" value='5' name="fequency_activity" required></td>
                                        <td><input type="radio" value='6' name="fequency_activity" required></td>
                                        <td><input type="radio" value='7' name="fequency_activity" required></td>
                                        <td><input type="radio" value='8' name="fequency_activity" required></td>
                                        <td><input type="radio" value='9' name="fequency_activity" required></td>
                                        <td><input type="radio" value='10' name="fequency_activity" required></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>5.- ¿Qué tan seguro se siente Ud. de poder evitar que la fatiga (el cansancio), debido a su artritis, interfiera con las cosas que quiere hacer?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name='fatigue' required></td>
                                        <td><input type="radio" value='2' name='fatigue' required></td>
                                        <td><input type="radio" value='3' name='fatigue' required></td>
                                        <td><input type="radio" value='4' name='fatigue' required></td>
                                        <td><input type="radio" value='5' name='fatigue' required></td>
                                        <td><input type="radio" value='6' name='fatigue' required></td>
                                        <td><input type="radio" value='7' name='fatigue' required></td>
                                        <td><input type="radio" value='8' name='fatigue' required></td>
                                        <td><input type="radio" value='9' name='fatigue' required></td>
                                        <td><input type="radio" value='10' name='fatigue' required></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>6.- ¿Qué tan seguro se siente Ud. de poder ayudarse a sí mismo a sentirse mejor si se siente triste?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='2' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='3' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='4' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='5' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='6' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='7' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='8' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='9' name='help_yourselft_sad' required></td>
                                        <td><input type="radio" value='10' name='help_yourselft_sad' required></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>7.- Comparándose con otras personas con artritis como la suya, ¿qué tan seguro se siente Ud. de poder sobrellevar el dolor de artritis durante sus actividades diarias?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name='daily_pain' required></td>
                                        <td><input type="radio" value='2' name='daily_pain' required></td>
                                        <td><input type="radio" value='3' name='daily_pain' required></td>
                                        <td><input type="radio" value='4' name='daily_pain' required></td>
                                        <td><input type="radio" value='5' name='daily_pain' required></td>
                                        <td><input type="radio" value='6' name='daily_pain' required></td>
                                        <td><input type="radio" value='7' name='daily_pain' required></td>
                                        <td><input type="radio" value='8' name='daily_pain' required></td>
                                        <td><input type="radio" value='9' name='daily_pain' required></td>
                                        <td><input type="radio" value='10' name='daily_pain' required></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>8.- ¿Qué tan seguro se siente Ud. de poder sobrellevar la frustración debido a su artritis?</strong></th>
                                        <td></td>
                                        <td><input type="radio" value='1' name='frustrations' required></td>
                                        <td><input type="radio" value='2' name='frustrations' required></td>
                                        <td><input type="radio" value='3' name='frustrations' required></td>
                                        <td><input type="radio" value='4' name='frustrations' required></td>
                                        <td><input type="radio" value='5' name='frustrations' required></td>
                                        <td><input type="radio" value='6' name='frustrations' required></td>
                                        <td><input type="radio" value='7' name='frustrations' required></td>
                                        <td><input type="radio" value='8' name='frustrations' required></td>
                                        <td><input type="radio" value='9' name='frustrations' required></td>
                                        <td><input type="radio" value='10' name='frustrations' required></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>   
                                <?= $this->Html->link('Volver',
                                    ['controller' => 'Pages', 'action' => 'list_form'],
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                                <?= $this->Form->button('Guardar', ['class' => 'btn btn-md btn-danger option','id' => 'save'],['escape'=>false]); ?>

                            </div>
                        <br>
                        <?= $this->Form->end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div>