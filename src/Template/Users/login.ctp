<section class="ilustracion_login">
    <div class="contenedor transparente">
        <div class="row">
            <div class="wrap">
                <div style="opacity:1 !important;">
                    <center>
                    <h3 style="font-family:avenir;">Si ya te registraste, coloca tu correo y tu clave</h3><?= $this->Form->create() ?>
                        <div class="input-group">
                            <?= $this->Form->control('email', [
                                'required' => true,
                                'placeholder' => 'Email',
                                'label' => 'Correo electrónico',
                                'class' => 'form-control',
                                'type' => 'text',
                                'style' => 'margin-left:-3%',
                            ]); ?>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <br>
                        <div class="input-group">
                            <?= $this->Form->control(
                                'password',
                                [
                                    'required' => true,
                                    'placeholder' => 'password',
                                    'label' => 'Contraseña',
                                    'class' => 'form-control',
                                    'type' => 'password',
                                    'style' => 'margin-left:-3%',
                                ]
                            ); ?>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                          
                        </div>
                        <br>

                        <div class="input-group">
                            <?= $this->Form->button('Enviar', [
                                'class' => 'btn btn-primary btn-block btn-flat'
                            ]) ?>

                        </div>

                        <!-- <h3 style="font-family:avenir;"><a href="/registrar">Si no registrate aquí</a></h3> -->

                        <h3 style="font-family:avenir;"> <?= $this->Html->link('Si no registrate aquí', ['controller' => 'Pages', 'action' => 'addUser'])?></h1>

                        <?= $this->Form->end() ?>
                        <?= $this->Html->link(
                            '¿Olvidó su contraseña?',
                            [
                                'controller' => 'Users',
                                'action' => 'recuperar'
                            ]
                            ); ?>
                        <br>
                        <div class="row">
                            <?= $this->Flash->render('auth') ?>
                            <?= $this->Flash->render() ?>
                        </div>
                        
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>
<!--?= $this->element("Layout/footer") ?-->