<?php
use Migrations\AbstractMigration;

class AlterTablePainAssessmentsAddColumnUserId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {$table = $this->table('pain_assessments');

        $table->addColumn('user_id', 'integer',[
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->update();
    }
}
