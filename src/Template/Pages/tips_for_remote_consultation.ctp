<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <style>
    ul{
        font-size: 15px;
    }
    h4{
        font-size: 18px;
        font-weight: bold;
    }
    .row{
        font-size: 12px;
        align-content: center;
    }
    li{
        border-bottom: 0px !important;
        border-top: 0px !important;
        border: 0px !important;
        float: none!important;
    }
   
 </style>
 
<!-- Page Content -->
<header class="masthead bg-primary text-white text-center">
	<img src="/img/header-landing.png" alt="" class="ilustracion5">
</header>
<div class="row">
    <div class="col-md-8 col-md-push-2">
            <li class="list-group-item" id="1">
                <h4 style="font-family: avenir;">9 Consejos para preparar una consulta remota (o teleatención)  <span style="font-size: 0.7em;"><a href="#0"></a></span></h4>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    1- Revisa que el equipo (computador, tablet o teléfono) con el que te vas a
                    conectar tenga:
                    <ol>
                        <li style="font-family: avenir; font-size:15px !important;"  class="list-group-item">- Suficiente carga de batería</li>
                        <li style="font-family: avenir; font-size:15px !important;"  class="list-group-item">- Buena señal de internet</li>
                        <li style="font-family: avenir; font-size:15px !important;"  class="list-group-item">- Cuente con el programa o aplicación que se usará para la atención</li>
                    </ol>
                </li>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    2- Busca un lugar apto para esta atención: lo más silencioso posible y con buena
                    iluminación.
                </li>
                <br>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    3- Ten papel y lápiz a mano para anotar indicaciones o recomendaciones que
                    recibas durante la atención.
                </li>
                <br>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    4- Sigue los consejos para preparar una consulta regular.
                </li>
                <br>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    5- Hay algunas cosas que no se podrán hacer igual que en una consulta
                    presencial (por ejemplo, medirte, pesarte, tomar tu presión). Si tienes cómo
                    hacerlo en casa, registra tu presión, peso, etc. (o algún indicador similar, como
                    talla de tu ropa).
                </li>
                <br>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    6- Si tienes que mostrar algún examen, es preferible que lo tengas en un formato
                    que te permita compartirlo de forma virtual (foto, documento pdf, etc.)
                </li>
                <br>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    7- Si hay alguna parte de tu cuerpo que necesitas que el profesional de la salud
                    vea (por ejemplo, por ronchas o manchas), asegúrate de usar ropa adecuada
                    que permita mostrar de manera rápida la zona del cuerpo que necesitas.
                </li>
                <br>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    8- Si por alguna razón el profesional de la salud se comunicó de su teléfono
                    personal, no asumas que eso te permite automáticamente establecer contacto
                    por otras vías distintas a la definida en la teleatención. Pregunta antes si lo
                    puedes contactar a ese número.
                </li>
                <br>
                <li style="font-family:avenir; font-size:15px !important;"  class="list-group-item">
                    9- Pide ayuda si sientes que la tecnología puede ser un impedimento para tu
                    buena atención.
                </li>
                <br>
            </li>
        </ul>
    </div>
</div>

<div class="col">
<center>
    <h2 style="font-size:25px;  margin-right:3%; margin-left:3%; font-family: avenir;" >
        Evalúa el estado de tu enfermedad hoy para que puedas compartirlo con tu doctor.
    </h2>
    <br>
    <a href="/list_form"><button type="button" class="btn" style="width:120px; height:40px; font-size:15px; color: white;background-color:#d3562e;">Aquí</button></a>
</center>
</div>
