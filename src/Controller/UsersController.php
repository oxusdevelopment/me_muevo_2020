<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\MailerAwareTrait;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */

class UsersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));

        /* $this->paginate = ['limit' => 10];
        $query = $this->Users
            ->find('all');
        $files = $this->paginate($query);*/
    }

    public function login()
    {
        if ($this->request->is(['post'])) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                $this->Flash->success(__('Te logeaste correctamente'));
                return $this->redirect(['controller' => 'Pages', 'action' => 'list_form']);
            }else{
                $this->Flash->error(__('El nombre de usuario o contraseña son incorrectos, por favor intente nuevamente.'));
            }
        }
    }

    public function loginRedirect()
    {
        $user = $this->currentUser;

        if($user['group_id'] == 2){
            return $this->redirect(['controller' => 'Pages', 'action' => 'list_form']);
        }else{
            return $this->redirect(['controller' => 'Pages', 'action' => 'list_form']);
        }     
        $this->set(compact('user'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);

        $this->set('user', $user);
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => '/index']);
    }

    public function recuperar ()
    {

    }

    public function changePassword()
    {
       
    }

use MailerAwareTrait;

    public function newPassword($emailToken)
    {
        $this->autoRender = false;
        $user = $this->Users->findByEmail_token($emailToken)->first();

        if (!isset($user) || !$user->isEmailTokenValid()) {
            $this->Flash->error('Ha expirado tiempo para cambiar contraseña, por favor solicítelo nuevamente.');
            return;
        }

        if ($this->request->is('post')) {
            $password = trim($this->request->data['Clave']);
            $rePassword = trim($this->request->data['Clave2']);

            if ($password == $rePassword) {
                
                $user->Clave = $password;

                $this->Users->save($user);

                $this->Flash->success(__('Su contraseña ha sido actualizada.'));

                return $this->redirect(['controller' => 'Users', 'action' => 'login']);
            } else {
                $this->Flash->error('Las contraseñas deben coincidir, inténtelo nuevamente.');
            }
        } $this->render('changePassword');
    }

    public function resetPassword()
    {
        $this->autoRender = false;
        $fecha = new \DateTime(date("Y-m-d H:i:s"));
        $fecha->modify("+12 hour");
        $fecha->format('Y-m-d H:i:s');

        if ($this->request->is('post')) {
            $email = trim($_POST['email']);
            $query = $this->Users->findByEmail($email);

            $user = $query->first();

            if ($user) {
                $hasher = new DefaultPasswordHasher();
                $token = $hasher->hash($user->email);

                $arr = array("/" => "");

                $token = strtr($token, $arr);

                $user->email_token = $token;
                $user->email_token_expires = $fecha;

                if ($this->Users->save($user)) {
                    $this->getMailer('User')->send('welcome', [$user, $_SERVER]);
                    $this->Flash->success(__('Se envio correo para recuperar contraseña.'));
                } else {
                    $this->Flash->error('Datos son invalidos, por favor intente nuevamente', ['Key' => 'Auth']);
                }
            } else {
                $this->Flash->error('Datos son invalidos, por favor intente nuevamente', ['Key' => 'Auth']);
            } 
        }   return $this->redirect(['action' => 'login']);
    }

    public function homePrivate(){
    }
}