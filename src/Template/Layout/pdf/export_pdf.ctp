<div class="col-md-12">
    <div class="container-fluid">
        <div class="row" style="margin-left:700px;">
            <? $creationDate = date("d-m-Y");
                print_r($creationDate);?>
        </div>
        <div class="col-md-10 col-md-push-1">
            <h2>Datos del Paciente</h2>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
               <li>
                    <strong>Nombre: </strong><?=$user->name;?>
               </li>
               <li>
                    <strong>Apellido: </strong><?=$user->lastname;?>
               </li>
               <li>
                   <strong>RUT: </strong><?=$user->rut;?>
               </li>
               <li>
                   <strong>Fecha de nacimiento: </strong><?=$user->birth_date?>
               </li>
               <li>
                   <strong>E-mail: </strong><?=$user->email?>
               </li>
            </div>
            <div class="col-md-6">
                <li>
                    <strong>Sexo: </strong> <?if($user->sex == 1){print_r('Femenino');}else if($user->sex == 2){print_r('Masculino');}else{print_r('No informado');}?>
                </li>
                <li>
                    <strong>Previsión: </strong><?if($user->healt == 1){print_r('Fonasa');}else if($user->healt == 2){print_r('Isapre');}else{print_r('No Informado');}?>
                </li>
                <li>
                    <strong>Teléfono: </strong><?=$user->phone;?>
                </li>
                <li>
                    <strong>Fecha de detección de la enfermedad: </strong> <?=$user->detection;?>
                </li>
            </div>
            <div class="col-md-12">
                <div class="container">
                    <h2 style="margin-left:6%;">Cuestionarios:</h2>
                    <h5>Capacidad Funcional:</h5>
                </div>
                <div class="col-md-12">
                    <!--?foreach($disability as $disable){?-->
                        <table class="table table-header-rotated" style="font-size: 12px;">
                            <thead>
                                <tr  class="success">
                                    <th><strong>¿Actualmente puede Ud:</strong></th>
                                    <th class="rotate"><div><strong>Respuesta paciente</strong></div></th>
                                </tr> 
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="row-header"><strong>1.- Vestirse, incluyendo amarrarse los zapatos y abrocharse(abotonarse)?</strong></th>
                                    <td><?=$disability->dress?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"><strong>2.- Acostarse y levantarse de la cama?</strong></th>
                                    <td><?=$disability->get_up?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"><strong>3.-Levantar hasta su boca una taza o vaso lleno?</strong></th>
                                    <td><?=$disability->lifting_cup?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"><strong>4.- Caminar al aire libre en terreno plano?</strong></th>
                                    <td><?=$disability->walking?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"><strong>5.- Bañarse y secarse todo el cuerpo?</strong></th>
                                    <td><?=$disability->swim?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"><strong>6.- Agacharse para recoger ropa del piso?</strong></th>
                                    <td><?=$disability->lifting_clothing?></td>

                                </tr>
                                <tr>
                                    <th class="row-header"><strong>7.- Abrir y cerrar las llaves de agua (los grifos)?</strong></th>
                                    <td><?=$disability->open_tap?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"><strong>8.- Subir y bajar del auto (carro)?</strong></th>
                                    <td><?=$disability->get_up_car?></td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-header-rotated" style="font-size: 12px;">
                            <thead>
                                <tr  class="success">
                                    <th class="rotate"><div><strong>N° of items</strong></div></th>
                                    <th class="rotate"><div><strong>Observed Range</strong></div></th>
                                    <th class="rotate"><div><strong>Mean</strong></div></th>
                                    <th class="rotate"><div><strong>Standard Deviation</strong></div></th>
                                    <th class="rotate"><div><strong>Internal Consistency Reliability</strong></div></th>
                                    <th class="rotate"><div><strong>Test-Retest Reliability</strong></div></th>
                                    <th class="rotate"><div><strong>Patient Result</strong></div></th>
                                </tr> 
                            </thead>
                            <tbody>
                                <tr>
                                    <td>8</td>
                                    <td>0-3</td>
                                    <td>1.7</td>
                                    <td>8</td>
                                    <td>.89</td>
                                    <td>.87</td>
                                    <td><?=($disability->dress+$disability->get_up+$disability->lifting_cup+$disability->walking+$disability->swim+$disability->lifting_clothing+$disability->open_tap+$disability->get_up_car)/8?></td>
                                </tr>
                            </tbody>
                        </table>
                    <!--?}?-->
                </div>
            </div>
        <div class="col-md-12">
            <div class="container">
                <h5>Percepción de salud social:</h5>
            </div>
            <div class="col-md-12">
                <?foreach($activity_limitation as $activity){?>
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th></th>
                                <th class="rotate"><div><strong>Respuesta Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                            <th class="row-header"><strong>1.- En sus actividades normales con sus familiares, amigos, vecinos o grupos:</strong></th>
                                <td><?=$activity->normal_activity?></td>
                            </tr>
                            <tr>
                            <th class="row-header"><strong>2.- En sus actividades recreativas o pasatiempos:</strong></th>
                                <td><?=$activity->recreational_activity?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>3.- En sus que haceres domésticos (tareas del hogar):</strong></th>
                                <td><?=$activity->housework?></td>
                            </tr>
                            <tr>
                            <th class="row-header"><strong>4.- En sus mandados / recados y compras:</strong></th>
                                <td><?=$activity->errand?></td>
                            </tr>
                        </tbody>
                    </table>  
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>N° of items</strong></div></th>
                                <th class="rotate"><div><strong>Observed Range</strong></div></th>
                                <th class="rotate"><div><strong>Mean</strong></div></th>
                                <th class="rotate"><div><strong>Standard Deviation</strong></div></th>
                                <th class="rotate"><div><strong>Internal Consistency Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Test-Retest Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Patient Result</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <td>4</td>
                                <td>0-4</td>
                                <td>1.08</td>
                                <td>1.10</td>
                                <td>.916</td>
                                <td>N/A</td>
                                <td><?=($activity->normal_activity+$activity->recreational_activity+$activity->housework+$activity->errand)/4?></td>
                            </tr>
                        </tbody>
                    </table>  
                <?}?>
            </div>
            <div class="container">
                <h5>Salud autopercibida:</h5>
            </div>
            <div class="col-md-12">
                <?foreach($self_rated_healt as $rated_healt){?> 
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr class="success">
                                <th></th>
                                <th class="rotate"><div><strong>Respuesta Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <th class="row-header"><strong>Generalmente, ¿Ud. diría que su salud es ... ?</strong></th>
                                <td><?=$rated_healt->general_point?></td>
                            </tr>
                        </tbody>
                    </table>  
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>N° of items</strong></div></th>
                                <th class="rotate"><div><strong>Observed Range</strong></div></th>
                                <th class="rotate"><div><strong>Mean</strong></div></th>
                                <th class="rotate"><div><strong>Standard Deviation</strong></div></th>
                                <th class="rotate"><div><strong>Internal Consistency Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Test-Retest Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Patient Result</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>1-5</td>
                                <td>4.04</td>
                                <td>0.772</td>
                                <td>-</td>
                                <td>0.87</td>
                                <td><?=($rated_healt->general_point)/1?></td>
                            </tr>
                        </tbody>
                    </table>  
                <?}?>
            </div>
            <div class="container">
                <h5>Comunicación con equipos de salud:</h5>
            </div>
            <div class="col-md-12">
                <?foreach($communication as $commun){?>
                <table class="table table-header-rotated" style="font-size: 10px;">
                    <thead>
                        <tr class="success">
                            <th></th>
                            <th class="rotate"><div><strong>Respuesta Paciente</strong></div></th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                            <th class="row-header"><strong>1.- Prepara una lista de preguntas para su médico:</strong></th>
                            <td><?=$commun->medial_question?></td>
                        </tr>
                        <tr>
                            <th class="row-header"><strong>2.- Hace preguntas acerca de las cosas que quiere saber y de las cosas que no entiende acerca de su tratamiento</strong></th>
                            <td><?=$commun->make_question?></td>
                        </tr>
                        <tr>
                            <th class="row-header"><strong>3.- Habla sobre algún problema personal que puede estar relacionado con su enfermedad</strong></th>
                            <td><?=$commun->personal_problem?></td>
                        </tr>
                    </tbody>
                </table> 
                <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>N° of items</strong></div></th>
                                <th class="rotate"><div><strong>Observed Range</strong></div></th>
                                <th class="rotate"><div><strong>Mean</strong></div></th>
                                <th class="rotate"><div><strong>Standard Deviation</strong></div></th>
                                <th class="rotate"><div><strong>Internal Consistency Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Test-Retest Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Patient Result</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <td>3</td>
                                <td>0-5</td>
                                <td>1.64</td>
                                <td>1.43</td>
                                <td>.732</td>
                                <td>N/A</td>
                                <td><?=($commun->medial_question+$commun->make_question+$commun->personal_problem)/3?></td>
                            </tr>
                        </tbody>
                    </table>  
                <?}?>  
            </div>
            <div class="container">
                <h5>Percepcion de autoeficacia:</h5>
            </div>
            <div class="col-md-12">
                <?foreach($arthritis as $arthri){?>
                    <table class="table table-header-rotated" style="font-size: 10px;">
                        <thead>
                            <tr class="success">
                                <th></th>
                                <th class="rotate"><div><strong>Respuesta Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <th class="row-header"><strong>1.- ¿Qué tan seguro se siente Ud. de poder reducir bastante su dolor?</strong></th>
                                <td><?=$arthri->reduce_pain?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>2.- ¿Qué tan seguro se siente Ud. de poder evitar que el dolor de la artritis interfiera con su sueño?</strong></th>
                                <td><?=$arthri->sleep_pain?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>3.-¿Qué tan seguro se siente Ud. de poder evitar que el dolor de la artritis interfiera con las cosas que quiere hacer?</strong></th>
                                <td><?=$arthri->pain_interfere?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>4.- ¿Qué tan seguro se siente Ud. de poder regular su actividad para mantenerse activo sin empeorar (agravar) su artritis?</strong></th>
                                <td><?=$arthri->fequency_activity?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>5.- ¿Qué tan seguro se siente Ud. de poder evitar que la fatiga (el cansancio), debido a su artritis, interfiera con las cosas que quiere hacer?</strong></th>
                                <td><?=$arthri->fatigue?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>6.- ¿Qué tan seguro se siente Ud. de poder ayudarse a sí mismo a sentirse mejor si se siente triste?</strong></th>
                                <td><?=$arthri->help_yourselft_sad?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>7.- Comparándose con otras personas con artritis como la suya, ¿qué tan seguro se siente Ud. de poder sobrellevar el dolor de artritis durante sus actividades diarias?</strong></th>
                                <td><?=$arthri->daily_pain?></td>
                            </tr>
                            <tr>
                                <th class="row-header"><strong>8.- ¿Qué tan seguro se siente Ud. de poder sobrellevar la frustración debido a su artritis?</strong></th>
                                <td><?=$arthri->frustrations?></td>
                            </tr>
                        </tbody>
                    </table>  
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>N° of items</strong></div></th>
                                <th class="rotate"><div><strong>Observed Range</strong></div></th>
                                <th class="rotate"><div><strong>Mean</strong></div></th>
                                <th class="rotate"><div><strong>Standard Deviation</strong></div></th>
                                <th class="rotate"><div><strong>Internal Consistency Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Test-Retest Reliability</strong></div></th>
                                <th class="rotate"><div><strong>Patient Result</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <td>8</td>
                                <td>1-10</td>
                                <td>5.9</td>
                                <td>2.1</td>
                                <td>.92</td>
                                <td>.69</td>
                                <td><?=($arthri->reduce_pain+$arthri->sleep_pain+$arthri->pain_interfere+$arthri->fequency_activity+$arthri->fatigue+$arthri->help_yourselft_sad+$arthri->daily_pain+$arthri->frustrations)/8?></td>
                            </tr>
                        </tbody>
                    </table>   
                <?}?>
            </div>
        </div>
    </div>
</div>