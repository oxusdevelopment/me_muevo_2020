<?php
use Migrations\AbstractMigration;

class AlterTableQuestionUsersAddRelationWithUsers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('question_users');
        $table->addForeignKey('user_id','users','id',['delete'=>'CASCADE','update'=>'NO_ACTION']);
        $table->update();

    }
}
