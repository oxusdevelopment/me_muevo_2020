<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h3 class="title">Valoracion del dolor</h3>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10 col-md-push-1" >
                        <?= $this->Form->create(); ?>
                            <table class="table table-header-rotated" style="font-size: 12px;">
                                <thead>
                                    <tr class="success">
                                        <th></th>
                                        <th class="rotate"><div><strong>0 ningun dolor</strong></div></th>
                                        <th class="rotate"><div><strong>1</strong></div></th>
                                        <th class="rotate"><div><strong>2</strong></div></th>
                                        <th class="rotate"><div><strong>3</strong></div></th>
                                        <th class="rotate"><div><strong>4</strong></div></th>
                                        <th class="rotate"><div><strong>5</strong></div></th>
                                        <th class="rotate"><div><strong>6</strong></div></th>
                                        <th class="rotate"><div><strong>7</strong></div></th>
                                        <th class="rotate"><div><strong>8</strong></div></th>
                                        <th class="rotate"><div><strong>9</strong></div></th>
                                        <th class="rotate"><div><strong>10 Demasiado dolor</strong></div></th>

                                    </tr> 
                                </thead>
                                <tbody>
                                    <p><strong>Por favor marque en la escala de abajo el número que mejor describa la intensidad de su dolor durante la última semana:</strong></p>
                                    <tr>
                                        <th class="row-header"></th>
                                        <td><input type="radio" value='0' name='pain_score' required></td>
                                        <td><input type="radio" value='1' name='pain_score' required></td>
                                        <td><input type="radio" value='2' name='pain_score' required></td>
                                        <td><input type="radio" value='3' name='pain_score' required></td>
                                        <td><input type="radio" value='4' name='pain_score' required></td>
                                        <td><input type="radio" value='5' name='pain_score' required></td>
                                        <td><input type="radio" value='6' name='pain_score' required></td>
                                        <td><input type="radio" value='7' name='pain_score' required></td>
                                        <td><input type="radio" value='8' name='pain_score' required></td>
                                        <td><input type="radio" value='9' name='pain_score' required></td>
                                        <td><input type="radio" value='10' name='pain_score' required></td>
                                    </tr>
                                </tbody>
                            </table>        
                                <?= $this->Html->link('Volver',
                                    ['controller' => 'Pages', 'action' => 'list_form'],
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                                <?= $this->Form->button('Guardar', ['class' => 'btn btn-md btn-danger option','id' => 'save'],['escape'=>false]); ?>
                        <?= $this->Form->end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>