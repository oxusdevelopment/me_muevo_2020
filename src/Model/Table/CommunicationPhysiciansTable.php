<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CommunicationPhysicians Model
 *
 * @method \App\Model\Entity\CommunicationPhysician get($primaryKey, $options = [])
 * @method \App\Model\Entity\CommunicationPhysician newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CommunicationPhysician[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CommunicationPhysician|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CommunicationPhysician saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CommunicationPhysician patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CommunicationPhysician[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CommunicationPhysician findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommunicationPhysiciansTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('communication_physicians');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('medial_question')
            ->allowEmptyString('medial_question');

        $validator
            ->integer('make_question')
            ->allowEmptyString('make_question');

        $validator
            ->integer('personal_problem')
            ->allowEmptyString('personal_problem');

        return $validator;
    }
}
