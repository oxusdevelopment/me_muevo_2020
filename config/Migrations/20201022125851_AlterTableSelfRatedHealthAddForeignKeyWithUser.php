<?php
use Migrations\AbstractMigration;

class AlterTableSelfRatedHealthAddForeignKeyWithUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('self_rated_health');
        $table->addForeignKey('user_id', 'users', 'id', array('delete' => 'NO_ACTION', 'update'=>'NO_ACTION'));
        $table->update();
    }
}
