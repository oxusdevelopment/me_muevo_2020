<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

/**
 * Default mailer.
 */
class DefaultMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Default';

    public function mail($args)
    {
        $this->to($args->to)
            ->profile('sendNotification')
            ->emailFormat('html')
            ->template($args->template)
            ->layout($args->layout)
            ->viewVars($args->viewVars)
            ->subject($args->subject)
            ->attachments($args->attachments);
    }
}
