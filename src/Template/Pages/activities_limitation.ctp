
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h3 class="title">Percepcion de salud social</h3>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10 col-md-push-1" >
                        <?= $this->Form->create(); ?>
                            <table class="table table-header-rotated" style="font-size: 12px;">
                                    <thead>
                                        <tr  class="success">
                                        <th></th>
                                        <th class="rotate"><div><strong>En nada</strong></div></th>
                                            <th class="rotate"><div><strong>Un poco</strong></div></th>
                                            <th class="rotate"><div><strong>En forma moderada</strong></div></th>
                                            <th class="rotate"><strong>La mayor parte del tiempo</strong></th>
                                            <th class="rotate"><strong>Todo el tiempo</strong></th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                    <p><strong>Durante la última semana, ¿cuánto ha interferido su salud en lo siguiente?</strong></p>
                                        <tr>
                                        <th class="row-header"><strong>1.- En sus actividades normales con sus familiares, amigos, vecinos o grupos:</strong></th>
                                        <td><input type="radio" value='0' name="normal_activity" required></td>
                                        <td><input type="radio" value='1' name="normal_activity" required></td>
                                        <td><input type="radio" value='2' name="normal_activity" required></td>
                                        <td><input type="radio" value='3' name="normal_activity" required></td>
                                        <td><input type="radio" value='4' name="normal_activity" required></td>
                                        </tr>
                                        <tr>
                                        <th class="row-header"><strong>2.- En sus actividades recreativas o pasatiempos:</strong></th>
                                        <td><input type="radio" value='0' name="recreational_activity" required></td>
                                        <td><input type="radio" value='1' name="recreational_activity" required></td>
                                        <td><input type="radio" value='2' name="recreational_activity" required></td>
                                        <td><input type="radio" value='3' name="recreational_activity" required></td>
                                        <td><input type="radio" value='4' name="recreational_activity" required></td>
                                        </tr>
                                        <tr>
                                        <th class="row-header"><strong>3.- En sus que haceres domésticos (tareas del hogar):</strong></th>
                                        <td><input type="radio" value='0' name="housework" required></td>
                                        <td><input type="radio" value='1' name="housework" required></td>
                                        <td><input type="radio" value='2' name="housework" required></td>
                                        <td><input type="radio" value='3' name="housework" required></td>
                                        <td><input type="radio" value='4' name="housework" required></td>
                                        </tr>
                                        <tr>
                                        <th class="row-header"><strong>4.- En sus mandados / recados y compras:</strong></th>
                                        <td><input type="radio" value='0' name="errand" required></td>
                                        <td><input type="radio" value='1' name="errand" required></td>
                                        <td><input type="radio" value='2' name="errand" required></td>
                                        <td><input type="radio" value='3' name="errand" required></td>
                                        <td><input type="radio" value='4' name="errand" required></td>
                                        </tr>
                                    </tbody>
                            </table>    
                                <?= $this->Html->link('Volver',
                                    ['controller' => 'Pages', 'action' => 'list_form'],
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                                <?= $this->Form->button('Guardar', ['class' => 'btn btn-md btn-danger option','id' => 'save'],['escape'=>false]); ?>
                        
                        <?= $this->Form->end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>