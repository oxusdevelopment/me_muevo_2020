<div class="col-md-12" style="padding:0;">
    <div class="contain">
        <img src="/img/header-landing.png" alt="" class="ilustracion5">
    </div>
</div>
<div class="col-md-12" style="padding:0px;">
    <div class="col-md-6" style="padding:0px;">
        <div class="contain">
            <div class="ilustracion d-flex justify-content-center" style="text-align:center">
                <h2 style="color:white; margin:0;" class="text_superior"> El mejor tratamiento para la artritis es el resultado de una buena conversacion con tu equipo de salud.</h2>
                <h2 style="color:white; margin:0;" class="text_superior"> En ella participas planteando tus dudas, necesidades, metas y juntos, deciden.</h2>
            </div>
        </div>
    </div>
    <div class="col-md-6" style="padding:0px;">
        <div class="contain" style="text-align:center;">
            <div class="imagen_podcast">
            <h1 style="margin:0;" class="podcast_text_2">Escucha nuestro podcast</h1>
            <h1 style="margin:0;" class="podcast_text">"TU VOZ IMPORTA"</h1>
            <h1 style="margin:0;" class="podcast_text_3">donde conversamos médicos y pacientes</h1>
            <center><a href="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/911679025&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true" target="_blank"><img src="/img/foto_lengua.png" alt=""  class="foto_lengua" style="position:relative;"></a></center>
            </div>
        </div>
    </div> 
</div>
<div class="col-md-12" id="form" >
	<center>
		<div style="margin-top: 60px;" class="text_evalua">
			<h2 style="margin-right:3%; margin-left:3%; font-family: Avenir" >
				Evalúa el estado de tu enfermedad hoy para que puedas compartirlo con tu doctor.
			</h2>
			<br>
			<a href="/list_form"><button type="button" class="btn" style="width:150px; height:50px; font-size:20px; color: white;background-color:#d3562e;">Aquí</button></a>
		</div>
	</center>
</div>
<div class="col-md-12" style="padding:0;">
    <div class="col-md-12" style="background-color:#fbdfd0;" id="icons" >
        <center>
        <a href="/list_form">
            <img src="/img/iconos/04.jpg"  class="img_home">
        </a>
        <a href="/advice">
            <img src="/img/iconos/02.jpg"  class="img_home">
        </a>
        <a href="/how_to_ask">
            <img src="/img/iconos/05.jpg"  class="img_home">
        </a>
        <br><br>
        <a href="/tips_to_prepare">
            <img src="/img/iconos/01.jpg"  class="img_home">
        </a>
        <a href="/maintaining_good_communication">
            <img src="/img/iconos/02.jpg"  class="img_home">
        </a>
        <a href="/tips_for_remote_consultation">
            <img src="/img/iconos/03.jpg"  class="img_home">
        </a>
        </center>
    </div>
</div>
<div>
    <div class="col-md-12">
        <section class="fondo-morado fila" id="servicio">
	        <div class="contenedor" style="text-align: center;">
				<div class="formulario" style=" margin-bottom: 40px;">
				<label class="label-cuentanos">Cuéntanos tu historia</label>            
				<?= $this->Form->create() ?> 
				<div class="col-md-10 col-md-push-1">
					<div class="form-group">
						<?= $this->Form->input('usernameusu',[
							'class'=>'form-control',
							'placeholder'=>'Nombre',
							'label'=>false,
							'required',
							'style' => 'font-family:Avenir'
						]) ?>
					</div>
				</div>    
				<br>
				<div class="col-md-10 col-md-push-1">
					<div class="form-group">
						<?= $this->Form->input('emailusu',[
							'class'=>'form-control',
							'placeholder'=>'Email',
							'label'=>false,
							'required',
							'style' => 'font-family:Avenir'
						]) ?> 
					</div>
				</div>	
				<br>
				<div class="col-md-10 col-md-push-1">
					<div class="form-group">
						<?= $this->Form->input('mensajeusu',[
							'class'=>'form-control',
							'placeholder'=>'Mensaje',
							'label'=>false,
							'required',
							'style' => 'font-family:Avenir'
						]) ?>
					</div>
				</div> 
				<br>
				<br>
				<br>
				<?= $this->Form->button('Enviar',[
					'class'=>'col-md-2 enviar enviar'
				])?>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="col-md-12" style="background-color:#eb8089;">
<section class="grid-container" >
	<div class="margin_text">
		<div class="grid-item">
			<h1 class="text_su_voz" Style="color:white; font-family: Avenir">Ellos y ellas, ya usaron su voz por la artritis</h1>
		</div>
	</div>
	<div class="reproductor_border">
		<div class="grid-item">
			<iframe width="100%" height="100" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/911679025&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/claudio-penailillo-703719223" title="FUNDACION MEMUEVO" target="_blank" style="color: #cccccc; text-decoration: none;">FUNDACION MEMUEVO</a> · <a href="https://soundcloud.com/claudio-penailillo-703719223/audio-de-prueba" title="Audio de Prueba" target="_blank" style="color: #cccccc; text-decoration: none;">Audio de Prueba</a></div>
			<iframe width="100%" height="100" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/911679025&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/claudio-penailillo-703719223" title="FUNDACION MEMUEVO" target="_blank" style="color: #cccccc; text-decoration: none;">FUNDACION MEMUEVO</a> · <a href="https://soundcloud.com/claudio-penailillo-703719223/audio-de-prueba" title="Audio de Prueba" target="_blank" style="color: #cccccc; text-decoration: none;">Audio de Prueba</a></div>
			<iframe width="100%" height="100" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/911679025&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/claudio-penailillo-703719223" title="FUNDACION MEMUEVO" target="_blank" style="color: #cccccc; text-decoration: none;">FUNDACION MEMUEVO</a> · <a href="https://soundcloud.com/claudio-penailillo-703719223/audio-de-prueba" title="Audio de Prueba" target="_blank" style="color: #cccccc; text-decoration: none;">Audio de Prueba</a></div>
		</div>
	</div>

</section>
</div>