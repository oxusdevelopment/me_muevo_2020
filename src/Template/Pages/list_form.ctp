<div class="col-md-12 background">
    <div class="container-fluid">
        <?= $this->Form->create();?>
            <h2 style="font-family:avenir;">Información básica de los pacientes</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->control('name', 
                            [
                                'required',
                                'placeholder' => 'Ingrese su Nombre',
                                'label' => '',
                                'class'=>"form-control",
                                'label' => 'Nombre',
                                'value' => isset($currentUser['name'])?$currentUser['name']:'',
                                'disabled' => isset($currentUser['name'])?true:false,
                            ]
                        );?>    
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->control('lastname', 
                            [
                                'required',
                                'placeholder' => 'Ingrese sus Apellidos',
                                'label' => 'Apellido',
                                'class'=>"form-control",
                                'value' => isset($currentUser['lastname'])?$currentUser['lastname']:'',
                                'disabled' => isset($currentUser['lastname'])?true:false,
                            ]
                        );?>  
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->control('document', 
                            [
                                'required',
                                'type' => 'Select',
                                'label' => 'Tipo de Documento',
                                'class'=>"form-control",
                                'empty' => 'Seleccione el tipo de documento',
                                'options' => [1 => 'Pasaporte', 2 => 'Cédula de identidad'],
                                'value' => isset($currentUser['document'])?$currentUser['document']:'',
                                'onlyread' => true,
                                'disabled' => isset($currentUser['document'])?true:false
                            ]
                        );?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->control('rut', 
                            [
                                'required',
                                'placeholder' => 'Ingrese su RUT o Pasaporte',
                                'label' => 'RUT',
                                'class'=>"form-control",
                                'value' => isset($currentUser['rut'])?$currentUser['rut']:'',
                                'disabled' => isset($currentUser['rut'])?true:false,
                            ]
                        );?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->input('birth_date', 
                            [
                                'required',
                                'label' => 'Fecha de nacimiento',
                                'class'=>"form-control",
                                'type' => 'text',
                                'value' => isset($currentUser['birth_date'])?$currentUser['birth_date']:'',
                                'disabled' => isset($currentUser['birth_date'])?true:false,
                                'id' => 'datetime',
                                'readonly'
                            ]
                        );?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->input('sex', 
                            [
                                'required',
                                'type' => 'select',
                                'empty' => 'seleccione su sexo',
                                'label' => 'Sexo',
                                'class'=>"form-control",
                                'options' => ['1' => 'Mujer', '2' => 'Hombre'],
                                'value' => isset($currentUser['sex'])?$currentUser['sex']:'',
                                'disabled' => isset($currentUser['sex'])?true:false,
                            ]
                        );?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->input('email', 
                            [
                                'required',
                                'label' => 'E-mail',
                                'class'=>"form-control", 
                                'value' => isset($currentUser['email'])?$currentUser['email']:'',
                                'disabled' => isset($currentUser['email'])?true:false,                       
                            ]
                        );?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->input('healt', 
                            [
                                'required',
                                'type' => 'select',
                                'empty' => 'seleccione su Previsión',
                                'label' => 'Previsión',
                                'class'=>"form-control",
                                'options' => ['1' => 'Fonasa', '2' => 'Isapre'],
                                'value' => isset($currentUser['healt'])?$currentUser['healt']:'',
                                'disabled' => isset($currentUser['healt'])?true:false,
                            ]
                        );?>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <?php echo $this->Form->input('phone',[
                            'required',
                            'label' => 'Número de teléfono',
                            'class'=>"form-control",
                            'value' => isset($currentUser['phone'])?$currentUser['phone']:'',
                            'disabled' => isset($currentUser['phone'])?true:false,
                        ]);?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <? echo $this->Form->input('detection',[
                            'required',
                            'label' => 'Fecha de detección de enfermedad',
                            'type' => 'date-picker',
                            'class' => 'form-control',
                            'value' => isset($currentUser['detection'])?$currentUser['detection']:'',
                            'disabled' => isset($currentUser['detection'])?true:false,
                            'readonly',
                            'id' => 'datetime2'
                        ]);?>
                    </div>
                </div>
            </div>
            <?= $this->Form->button(
                '<i class="fa fa-floppy-o fa-side"></i> Guardar',
                ['class' => 'btn btn-md btn-danger option',  'type' => 'submit'],
                [
                    'action' => 'index',
                    'Controller' => 'Tariffs',
            ]); ?>
        <?= $this->Form->end() ?> 
    </div>
</div>
<div class="col-md-12 background2">
    <h2 style="color:white; font-family:avenir;margin-left:9%;">Autoevaluación de la enfermedad</h2>
    <div class="col-md-10 col-md-push-1 background3">
        <div class="row">
            <div class="col-md-6">
                <div class="container-fluid">
                    <h1 style="color:white; font-family:avenir;"> <?= $this->Html->link('1.- Salud Autopercibida', ['controller' => 'Pages', 'action' => 'selfRatedHealt'])?></h1>
                </div>
            </div>
            <div class="col-md-2 col-md-push-1">
                <div class="form-group">
                    <?php echo $this->Form->control('', 
                        [
                            'required',
                            'placeholder' => '',
                            'label' => '',
                            'class'=>"form-control date-test",
                            'label' => ' ',
                            'value' => isset($self_rated_healt->modified)?$self_rated_healt->modified->format('d-m-Y'):'',
                            'disabled'
                        ]
                    );?>    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="container-fluid">
                    <h1 style="color:white; font-family:avenir;"> <?= $this->Html->link('2.- Valoracion del dolor', ['controller' => 'Pages', 'action' => 'selfRatedHealt'])?></h1>
                </div>
            </div>
            <div class="col-md-2 col-md-push-1">
                <div class="form-group">
                    <?php echo $this->Form->control('', 
                        [
                            'required',
                            'placeholder' => '',
                            'label' => '',
                            'class'=>"form-control date-test",
                            'label' => ' ',
                            'value' => isset($self_rated_healt->modified)?$self_rated_healt->modified->format('d-m-Y'):'',
                            'disabled'
                        ]
                    );?>    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="container-fluid">
                    <h1 style="color:white; font-family:avenir;"> <?= $this->Html->link('3.- Capacidad Funcional', ['controller' => 'Pages', 'action' => 'disabilityScale'])?></h1>
                </div>
            </div>
            <div class="col-md-2 col-md-push-1">
                <div class="form-group">
                    <?php echo $this->Form->control('', 
                        [
                            'required',
                            'placeholder' => '',
                            'label' => '',
                            'class'=>"form-control date-test",
                            'label' => '',
                            'value' => isset($disability->modified)?$disability->modified->format('d-m-Y'):'',
                            'disabled'
                        ]
                    );?>    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="container-fluid">
                    <h1 style="color:white; font-family:avenir;"> <?= $this->Html->link('4.- Percepcion de autoeficacia', ['controller' => 'Pages', 'action' => 'arthritisSelfEfficacy'])?></h1>
                </div>
            </div>
            <div class="col-md-2 col-md-push-1">
                <div class="form-group">
                    <?php echo $this->Form->control('', 
                        [
                            'required',
                            'placeholder' => '',
                            'label' => 'Fecha último test',
                            'class'=>"form-control date-test",
                            'label' => ' ',
                            'value' => isset($arthritis->modified)?$arthritis->modified->format('d-m-Y'):'',
                            'disabled'
                        ]
                    );?>    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="container-fluid">
                    <h1 style="color:white; font-family:avenir;"> <?= $this->Html->link('5.- Percepción de salud social', ['controller' => 'Pages', 'action' => 'activitiesLimitation'])?></h1>
                </div>
            </div>
            <div class="col-md-2 col-md-push-1">
                <div class="form-group">
                    <?php echo $this->Form->control('', 
                        [
                            'required',
                            'placeholder' => '',
                            'label' => '',
                            'class'=>"form-control date-test",
                            'label' => ' ',
                            'value' => isset($activity_limitation->modified)?$activity_limitation->modified->format('d-m-Y'):'',
                            'disabled'
                        ]
                    );?>    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="container-fluid">
                    <h1 style="color:white; font-family:avenir;"> <?= $this->Html->link('6.- Comunicación con equipos de salud', ['controller' => 'Pages', 'action' => 'communicationWithPhysicians'])?></h1>
                </div>
            </div>
            <div class="col-md-2 col-md-push-1">
                <div class="form-group">
                    <?php echo $this->Form->control('', 
                        [
                            'required',
                            'placeholder' => '',
                            'label' => '',
                            'class'=>"form-control date-test",
                            'label' => ' ',
                            'value' => isset($communication->modified)?$communication->modified->format('d-m-Y'):'',
                            'disabled'
                        ]
                    );?>    
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8 background2">
        <center>
            <h1 style="color:white; font-family:avenir;"> 
                <?= $this->Html->link(' <i  class="fa fa-file"></i> Descargar pruebas para su doctor',[
                    'controller' => 'Pages',
                    'action' => 'exportPdf',
                    '_ext' => 'pdf'
                ],[
                    'class' => 'btn btn-box-tool btn-pdf', 
                    'escape' => false, 
                    'title' => 'Descargar PDF'
                ]);?>  
            </h1>
        </center>
    </div>
</div>

<script>
    $('#datetime').datetimepicker({
        format: "d-m-Y", dayOfWeekStart:1,
        timepicker:false,
    });
    $('#datetime2').datetimepicker({
        format: "d-m-Y", dayOfWeekStart:1,
        timepicker:false,
    });
</script>

