<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DisabilityScales Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $Users
 * @property |\Cake\ORM\Association\HasMany $QuestionUsers
 *
 * @method \App\Model\Entity\DisabilityScale get($primaryKey, $options = [])
 * @method \App\Model\Entity\DisabilityScale newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DisabilityScale[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DisabilityScale|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DisabilityScale saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DisabilityScale patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DisabilityScale[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DisabilityScale findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DisabilityScalesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('disability_scales');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('QuestionUsers', [
            'foreignKey' => 'disability_scale_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('dress')
            ->allowEmptyString('dress');

        $validator
            ->integer('get_up')
            ->allowEmptyString('get_up');

        $validator
            ->integer('lifting_cup')
            ->allowEmptyString('lifting_cup');

        $validator
            ->integer('walking')
            ->allowEmptyString('walking');

        $validator
            ->integer('swim')
            ->allowEmptyString('swim');

        $validator
            ->integer('lifting_clothing')
            ->allowEmptyString('lifting_clothing');

        $validator
            ->integer('open_tap')
            ->allowEmptyString('open_tap');

        $validator
            ->integer('get_up_car')
            ->allowEmptyString('get_up_car');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
