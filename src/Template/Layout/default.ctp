<?php
$cakeDescription = 'Campaña Artritis 2020';
?>
<!DOCTYPE html>
<html>
    <head>
        <?= $this->Html->charset() ?>
        <?= $this->Flash->render() ?>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            <!-- CSRF Token -->
        <meta name="csrf-token" content="mY9uGWBCgunlkaraBxAPxhwtFAek3g5Zo3192ILa">
        <meta name="google-site-verification" content="CXybBQoIWc2aeFrdt62LN4-Tov4psPVS7vzt5wKzX78" />
        <title>
            <?= $cakeDescription ?>
        </title>
        <!-- <link rel="shortcut icon" href="/img/logo.png">      -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
        <title>Me muevo</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/css/reset.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/fontello.css">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="/js/funciones.js"></script>
        <script type="text/javascript" src="/js/js/jquery.datetimepicker.full.min.js"></script>
        <link rel="stylesheet" href="/css/jquery.datetimepicker.min.css">

    </head>

    <body class="skin-blue sidebar-mini sidebar-collapse">
        <div class="wrapper">
            <?
                if (isset($user)) {
                    echo $this->element('/Layout/header-private');
                } else {
                    echo $this->element("Layout/header");
                }
            ?>

            <div class="content-wrapper">
                <!-- Main content -->
                <section class="content"> 
                    <?= $this->fetch('content') ?>
                </section>
                <!-- /.content -->
            </div>

            <?=$this->element("Layout/footer")?>
        </div>
    </body>
</html>