<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * QuestionUser Entity
 *
 * @property int $id
 * @property int|null $disability_scale_id
 * @property int|null $activity_limitation_id
 * @property int|null $self_rated_health_id
 * @property int|null $communication_physician_id
 * @property int|null $arthritis_selft_efficacy_id
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 *
 * @property \App\Model\Entity\DisabilityScale $disability_scale
 * @property \App\Model\Entity\ActivityLimitation $activity_limitation
 * @property \App\Model\Entity\SelfRatedHealth $self_rated_health
 * @property \App\Model\Entity\CommunicationPhysician $communication_physician
 * @property \App\Model\Entity\ArthritisSelftEfficacy $arthritis_selft_efficacy
 * @property \App\Model\Entity\User $user
 */
class QuestionUser extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
    ];
}
