<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <style>
    ul{
        font-size: 15px;
    }
    h4{
        font-size: 18px;
        font-weight: bold;
    }
    .row{
        font-size: 12px;
        align-content: center;
    }
    li{
        border-bottom: 0px !important;
        border-top: 0px !important;
        border: 0px !important;
        float: none!important;
    }
   
 </style>
 
<!-- Page Content -->
<header class="masthead bg-primary text-white text-center">
	<img src="/img/header-landing.png" alt="" class="ilustracion5">
</header>
<div class="row">
    <div class="col-md-8 col-md-push-2">
        <li class="list-group-item" id="1">
            <h4 style="font-family: avenir;">Manteniendo una buena comunicación con el equipo de salud <span style="font-size: 0.7em;"><a href="#0"></a></span></h4>
            <p style="font-family: avenir; font-size:15px !important;">
                Cuando tenemos una enfermedad crónica, el equipo de salud cumple un rol importante
                en el manejo de nuestra condición. Ellos nos acompañarán en los buenos y en los
                malos momentos y juntos iremos aprendiendo más de cómo esta condición se
                desarrolla en mí. Necesitamos entonces lograr una buena comunicación con nuestro
                equipo de salud.
                <br>
                Cada uno de nosotros quiere ser tratado como una persona única (porque lo somos) y
                que nuestras necesidades y preocupaciones sean escuchadas. Algo que puede
                jugarnos en contra de eso es el poco tiempo que tenemos para la atención. También
                es bueno recordar que el equipo de salud no nos atiende sólo a nosotros en el día. A
                veces seremos la persona número 20 o 30 con quien un profesional de la salud debe
                interactuar.
                <br>
                ¿Cómo mantener una buena comunicación en este contexto?
                <br>
                Siempre es bueno recordar que somos dos personas (o más) las que nos
                encontramos en ese espacio de la consulta, más allá de los títulos o roles. Todas las
                personas tenemos preocupaciones y podemos tener algunos días mejores que otros.
                Pero como se trata de nuestra salud, podemos tomar la iniciativa para que esa
                interacción sea buena. ¿Cómo? Saludando, hablando claro, preparando previamente
                la consulta, expresando nuestras necesidades, etc.
                <br>
                Un paciente informado que puede expresar sus necesidades, estará en mejores
                condiciones de colaborar con el equipo de salud.
                <br>
                Recuerda que si recibes un trato inadecuado, siempre podemos manifestar nuestra
                opinión a través de los formularios que están dispuestos en todos los centros de
                atención (como por ejemplo, las OIRS).
            </p>
        </li>
    </div>
</div>

<div class="col">
<center>
    <h2 style="font-size:25px;  margin-right:3%; margin-left:3%; font-family: avenir;" >
        Evalúa el estado de tu enfermedad hoy para que puedas compartirlo con tu doctor.
    </h2>
    <br>
    <a href="/list_form"><button type="button" class="btn" style="width:120px; height:40px; font-size:15px; color: white;background-color:#d3562e;">Aquí</button></a>
</center>
</div>
