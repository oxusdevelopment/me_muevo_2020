<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Http\Exception\ForbiddenException;
use Cake\Http\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\FrozenTime;
use Cake\Mailer\Email;
use  Cake\Mailer\divorcioEnLinea;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    /**
     * Displays a view
     *
     * @param array ...$path Path segments.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Http\Exception\ForbiddenException When a directory traversal attempt.
     * @throws \Cake\Http\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display(...$path)
    {
        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadModel('ActivityLimitations');
        $this->loadModel('SelfRatedHealth');
        $this->loadModel('CommunicationPhysicians');
        $this->loadmodel('ArthritisSelftEfficacies');
        $this->loadModel('Users');
        $this->loadmodel('DisabilityScales');
        $this->loadModel('PainAssessments');
    }

    public function home()
    {
        if($this->request->is('post')){
        $formulario = $this->request->data();

        $mail = 'hugo.hjpb@gmail.com';
        $title = 'Contactanos Me Muevo - Volar';
        $message = [
            'usuario' => $formulario['usernameusu'],
            'email' => $formulario['emailusu'],
            'mensaje' => $formulario['mensajeusu']
        ];

        $this->sendNotification($mail, "default", "default", compact("title", "message"),$title, null, false);

        }
    }

    public function addUser()
    {
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->group_id = 2;
            if ($this->Users->save($user)) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    $this->Flash->success(__('Te logeaste correctamente'));
                }
                $this->Flash->success(__('El usuario se guardó exitosamente.'));
                return $this->redirect(['controller' => 'Pages', 'action' => 'listForm']);
            }else{
                $this->Flash->error(__('El usuario no pudo guardarse, por favor, intente nuevamente..'));
                return $this->redirect($this->referer());
            }
            
        }
        $this->set(compact('user'));
    }

    public function resetPassword()
    { 

    }

    public function listForm($id = null){

        $user = $this->Users->get($this->currentUser['id']);

        $disability = $this->DisabilityScales->find('all')
        ->where(['DisabilityScales.user_id' => $user->id])
        ->last();

        $activity_limitation = $this->ActivityLimitations->find('all')
        ->where(['ActivityLimitations.user_id' => $user->id])
        ->last();

        $self_rated_healt = $this->SelfRatedHealth->find('all')
        ->where(['SelfRatedHealth.user_id' => $user->id])
        ->last();

        $communication = $this->CommunicationPhysicians->find('all')
        ->where(['CommunicationPhysicians.user_id' => $user->id])
        ->last();

        $arthritis = $this->ArthritisSelftEfficacies->find('all')
        ->where(['ArthritisSelftEfficacies.user_id' => $user->id])
        ->last();

        $pain = $this->PainAssessments->find('all')
        ->where(['PainAssessments.user_id' => $user->id])
        ->last();

        if($this->request->is(['patch', 'post', 'put'])){
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)){
                $this->Flash->success(__('Se han actualizado sus datos correctamernte.'));
                return $this->redirect(['action' => 'listForm']); 
            }else{
                $this->Flash->error(__('Sus datos no pudieron ser actualizados, por favor intente nuevamente.'));
            }
        }
        $this->set(compact('user', 'disability', 'activity_limitation', 'self_rated_healt', 'communication', 'arthritis', 'pain'));
    }

    public function disabilityScale(){

        $disability = $this->DisabilityScales->find('all');

        $form = $this->DisabilityScales->newEntity();
        if($this->request->is(['past', 'post', 'put'])){
            $form = $this->DisabilityScales->patchEntity($form, $this->request->getData());
            $form->user_id = $this->currentUser['id'];
            if($this->DisabilityScales->save($form)){
                $this->Flash->success(__('Formulario ha sido creado correctamente.'));
                return $this->redirect(['action' => 'listForm']);
            }else{
                $this->Flash->error(__('Foprmulario no pudo ser guardado, por favor intente nuevamente'));
            }
        }
        $this->set(compact('disability'));
    }

    public function activitiesLimitation(){
        $form = $this->ActivityLimitations->newEntity();
        if($this->request->is(['past', 'post', 'put'])){
            $form = $this->ActivityLimitations->patchEntity($form, $this->request->getData());
            $form->user_id = $this->currentUser['id'];
            if($this->ActivityLimitations->save($form)){
                $this->Flash->success(__('Formulario ha sido creado correctamente.'));
                return $this->redirect(['action' => 'listForm']);            
            }else{
                $this->Flash->error(__('Formulario no pudo ser guardado, por favor intente nuevamente.'));
            }
        }

    }

    public function selfRatedHealt(){
        $form = $this->SelfRatedHealth->newEntity();

        if($this->request->is(['patch', 'post', 'put'])){
            $form = $this->SelfRatedHealth->patchentity($form, $this->request->getData());
            $form->user_id = $this->currentUser['id'];
            if($this->SelfRatedHealth->save($form)){
                $this->Flash->success(__('Formulario ha sido guardado correctamente.'));
                return $this->redirect(['action' => 'listForm']);            

            }else{
                $this->Flash->error(__('Formulario no pudo ser guardado, por favor intente nuevamente.'));
            }
        }

    }

    public function communicationWithPhysicians(){
        $form = $this->CommunicationPhysicians->newEntity();

        if($this->request->is(['patch', 'post', 'put'])){
            $form = $this->CommunicationPhysicians->patchEntity($form, $this->request->getData());
            $form->user_id = $this->currentUser['id'];
            if($this->CommunicationPhysicians->save($form)){
                return $this->redirect(['action' => 'listForm']);
                $this->Flash->success(__('Formulario ha sido guardado con exito.'));
            }else{
                $this->Flash->error(__('Formulario no pudo ser guardado, por favor intente nuevamente.'));
            }
        }

    }

    public function arthritisSelfEfficacy(){
        $form = $this->ArthritisSelftEfficacies->newEntity();

        if($this->request->is(['patch', 'post', 'put'])){
            $form = $this->ArthritisSelftEfficacies->patchEntity($form, $this->request->getData());
            $form->user_id = $this->currentUser['id'];
            if($this->ArthritisSelftEfficacies->save($form)){
                $this->Flash->success(__('Formulario ha sido guardado con exito.'));
                return $this->redirect(['action' => 'listForm']);
            }else{
                $this->Flash->error(__('Formulario no pudo ser guardado, por favor intente nuevamente.'));
            }
        }
    }

    public function painAssessment(){
        $form = $this->PainAssessments->newEntity();

        if($this->request->is(['patch', 'post', 'put'])){
            $form = $this->PainAssessments->patchEntity($form, $this->request->getData());
            $form->user_id = $this->currentUser['id'];
            if($this->PainAssessments->save($form)){
                $this->Flash->success(__('Formulario ha sido guardado con exito.'));
                return $this->redirect(['action' => 'listForm']);
            }else{
                $this->Flash->error(__('Formulario no pudo ser guardado, por favor intente nuevamente.'));
            }
        }
    }

    public function exportPdf($id = null){

        $user = $this->Users->get($this->currentUser['id']);

        $disability = $this->DisabilityScales->find('all')
        ->where(['DisabilityScales.user_id' => $user->id])
        ->last();

        $activity_limitation = $this->ActivityLimitations->find('all')
        ->where(['ActivityLimitations.user_id' => $user->id])
        ->last();

        $self_rated_healt = $this->SelfRatedHealth->find('all')
        ->where(['SelfRatedHealth.user_id' => $user->id])
        ->last();

        $communication = $this->CommunicationPhysicians->find('all')
        ->where(['CommunicationPhysicians.user_id' => $user->id])
        ->last();

        $arthritis = $this->ArthritisSelftEfficacies->find('all')
        ->where(['ArthritisSelftEfficacies.user_id' => $user->id])
        ->last();

        $pain = $this->PainAssessments->find('all')
        ->where(['PainAssessments.user_id' => $user->id])
        ->last();

        $this->viewBuilder()->options([
            'pdfConfig' => [
                'orientation' => 'portrait',
                'filename' => 'Detalles_cuestionarios.pdf',
                'margin' => [
                    'top' => 5,
                    'bottom' => 5,
                    'left' => 5,
                    'right' => 5,
                ],
            ]
        ]);

        $this->set(compact('user','disability','activity_limitation','self_rated_healt','communication','arthritis', 'pain'));
    }

    public function advice(){
        
    }

    public function howToAsk(){

    }

    public function tipsToPrepare(){

    }

    public function maintainingGoodCommunication(){

    }

    public function tipsForRemoteConsultation (){

    }
    
}
