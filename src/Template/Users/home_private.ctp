

<div class="container">
    <br>
    <div class="row" id="0">
        <div class="col-sm-4"><a href="#">Formulario</a></div>
        <div class="col-sm-4"><a href="#1">Consejos para monitorear tus síntomas</a></div>
        <div class="col-sm-4"><a href="#2">¿Cómo hacer buenas preguntas en la consulta?</a></div>
    </div>
    <div class="row">
        <div class="col-sm-4"><a href="#3">6 consejos para preparar una consulta</a></div>
        <div class="col-sm-4"><a href="#4">Manteniendo una buena comunicación con el equipo de salud</a></div>
        <div class="col-sm-4"><a href="#5">9 Consejos para preparar una consulta remota (o teleatención)</a></div>
    </div>
    <br>
    <ul class="list-group list-group-flush">
        <li class="list-group-item" id="1">
            <h4>Consejos para monitorear tus síntomas <span style="font-size: 0.7em;"><a href="#0">Volver</a></span></h4>
                Una buena práctica que llevan a cabo las personas activas en el manejo de su
                condición de salud es el monitoreo de sus síntomas entre cada consulta. De esta
                forma pueden reportar con detalle los cambios que puedan haber ocurrido en ese
                tiempo.
                <br>Para esto, es bueno que puedas tener una libreta o cuaderno donde vayas escribiendo
                tus reflexiones cada cierto tiempo. Algunas preguntas interesantes de hacerse son:
                <ul>
                    <li>
                        Tus síntomas en términos generales ¿están mejor, peor o igual?
                    </li>
                    <li> 
                        Si has tenido cambios en tus síntomas (es decir, si han aumentado, disminuido
                        o cambiado), estos cambios ¿han sido lentos o rápidos?
                    </li>
                    <li> 
                        ¿Ha cambiado o ha pasado algo en tu vida que te pueda estar afectando?
                    </li>
                    <li> 
                        No olvides escribir sobre las cosas que haces para ayudarte a mejorar tus
                        síntomas.
                    </li>
                </ul>
                <br>Y recuerda: no es necesario registrar esta información todos los días, tampoco
                queremos vivir sólo ocupados de nuestra enfermedad. Pero sí es bueno escribir cada
                cierto tiempo o cuando ocurran cambios importantes.
                Estas reflexiones puedes compartirlas cuando vayas a control con tu equipo de salud.
                Así podrán saber más de ti durante todo el tiempo en que no se vieron.
        </li>
        <li class="list-group-item" id="2">
            <h4>¿Cómo hacer buenas preguntas en la consulta? <span style="font-size: 0.7em;"><a href="#0">Volver</a></span></h4>
                Hacer preguntas en la consulta nos va a ayudar a entender mejor lo que nos está
                pasando y las acciones que debemos realizar a futuro. Pero muchas veces, en el
                momento nos quedamos en blanco y al llegar a la casa vuelven a aparecer todas las
                dudas.
                Estos simples consejos pueden ayudarte a preparar tus preguntas:
                <ul>
                    <li>
                        Escribe una lista con las preocupaciones o preguntas para hacer a tu equipo de
                        salud (médico, enfermera, terapeuta, etc.).
                    </li>
                    <li>
                        Marca o destaca las que tú consideras más importantes.
                    </li>
                    <li>
                        Al inicio de la consulta, entrega tu listado al profesional de la salud. Esto es
                        clave porque lo más probable es que no pueda responderlas en su totalidad
                        por el tiempo acotado de la atención. Pero al mirarlas todas, podrá seleccionar
                        aquellas que mayor impacto pueden tener en tu salud.
                    </li>
                </ul>
                ¡No dejes de preparar tus preguntas para tu siguiente consulta!
        </li>
        <li class="list-group-item" id="3">
            <h4>6 consejos para preparar una consulta <span style="font-size: 0.7em;"><a href="#0">Volver</a></span></h4>
                No es novedad si te decimos que el tiempo de la consulta es bastante breve y,
                lamentablemente, eso no depende de nosotros como pacientes. Lo que sí está en
                nuestras manos, es usar ese tiempo de la mejor manera posible.
                <ol>
                    <li>
                        Lleva una lista de todos los medicamentos que tomas y sus dosis
                        correspondientes (incluye los que compras sin receta, hierbas y/o vitaminas).
                    </li>
                    <li>
                        Reporta tus síntomas y molestias de una forma clara y concisa (puedes seguir
                        los consejos sobre el monitoreo de síntomas para esto).
                    </li>
                    <li>
                        Habla de lo que más te preocupa acerca de esos síntomas y sobre lo que
                        piensas que los está causando.
                    </li>
                    <li>
                        Si estás usando un nuevo medicamento o tratamiento, prepara un breve
                        reporte de cómo te ha ido y cuáles han sido los efectos.
                    </li>
                    <li>
                        Comparte tus emociones, sentimientos y temores de la manera más clara
                        posible.
                    </li>
                    <li>
                        Pregunta para confirmar lo que entendiste sobre el diagnóstico, exámenes,
                        tratamientos y seguimiento.
                    </li>
                </ol><br>
                Recuerda que necesitas tener la información necesaria para poder tomar decisiones
                que tienen que ver con tu salud. Si crees que necesitas apoyo, invita a alguien de tu
                confianza para ir a la consulta contigo, así puede registrar lo que conversen o
                acuerden con el profesional de la salud.
        </li>
        <li class="list-group-item" id="4">
            <h4>Manteniendo una buena comunicación con el equipo de salud <span style="font-size: 0.7em;"><a href="#0">Volver</a></span></h4>
                Cuando tenemos una enfermedad crónica, el equipo de salud cumple un rol importante
                en el manejo de nuestra condición. Ellos nos acompañarán en los buenos y en los
                malos momentos y juntos iremos aprendiendo más de cómo esta condición se
                desarrolla en mí. Necesitamos entonces lograr una buena comunicación con nuestro
                equipo de salud.
                <br>Cada uno de nosotros quiere ser tratado como una persona única (porque lo somos) y
                que nuestras necesidades y preocupaciones sean escuchadas. Algo que puede
                jugarnos en contra de eso es el poco tiempo que tenemos para la atención. También
                es bueno recordar que el equipo de salud no nos atiende sólo a nosotros en el día. A
                veces seremos la persona número 20 o 30 con quien un profesional de la salud debe
                interactuar.
                <br>¿Cómo mantener una buena comunicación en este contexto?
                <br>
                Siempre es bueno recordar que somos dos personas (o más) las que nos
                encontramos en ese espacio de la consulta, más allá de los títulos o roles. Todas las
                personas tenemos preocupaciones y podemos tener algunos días mejores que otros.
                Pero como se trata de nuestra salud, podemos tomar la iniciativa para que esa
                interacción sea buena. ¿Cómo? Saludando, hablando claro, preparando previamente
                la consulta, expresando nuestras necesidades, etc.
                <br>Un paciente informado que puede expresar sus necesidades, estará en mejores
                condiciones de colaborar con el equipo de salud.
                <br>Recuerda que si recibes un trato inadecuado, siempre podemos manifestar nuestra
                opinión a través de los formularios que están dispuestos en todos los centros de
                atención (como por ejemplo, las OIRS).
        </li>
        <li class="list-group-item" id="5">
            <h4>9 Consejos para preparar una consulta remota (o teleatención) <span style="font-size: 0.7em;"><a href="#0">Volver</a></span></h4>
                <ol>
                    <li>
                        Revisa que el equipo (computador, tablet o teléfono) con el que te vas a
                        conectar tenga:
                        <ul>
                            <li>Suficiente carga de batería</li>
                            <li>Buena señal de internet</li>
                            <li>Cuente con el programa o aplicación que se usará para la atención</li>
                        </ul>
                    </li>
                    
                    <li>
                        Busca un lugar apto para esta atención: lo más silencioso posible y con buena
                        iluminación.
                    </li>
                    <li>
                        Ten papel y lápiz a mano para anotar indicaciones o recomendaciones que
                        recibas durante la atención.
                    </li>
                    <li>
                        Sigue los consejos para preparar una consulta regular.
                    </li>
                    <li>
                        Hay algunas cosas que no se podrán hacer igual que en una consulta
                        presencial (por ejemplo, medirte, pesarte, tomar tu presión). Si tienes cómo
                        hacerlo en casa, registra tu presión, peso, etc. (o algún indicador similar, como
                        talla de tu ropa).
                    </li>
                    <li>
                        Si tienes que mostrar algún examen, es preferible que lo tengas en un formato
                        que te permita compartirlo de forma virtual (foto, documento pdf, etc.)
                    </li>
                    <li>
                        Si hay alguna parte de tu cuerpo que necesitas que el profesional de la salud
                        vea (por ejemplo, por ronchas o manchas), asegúrate de usar ropa adecuada
                        que permita mostrar de manera rápida la zona del cuerpo que necesitas.
                    </li>
                    <li>
                        Si por alguna razón el profesional de la salud se comunicó de su teléfono
                        personal, no asumas que eso te permite automáticamente establecer contacto
                        por otras vías distintas a la definida en la teleatención. Pregunta antes si lo
                        puedes contactar a ese número.
                    </li>
                    <li>
                        Pide ayuda si sientes que la tecnología puede ser un impedimento para tu
                        buena atención.
                    </li>
                </ol>
        </li>
    </ul>
</div>