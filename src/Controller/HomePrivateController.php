<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * HomePrivate Controller
 *
 *
 * @method \App\Model\Entity\HomePrivate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HomePrivateController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // $homePrivate = $this->paginate($this->HomePrivate);

        // $this->set(compact('homePrivate'));
    }

    /**
     * View method
     *
     * @param string|null $id Home Private id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function view($id = null)
    // {
    //     $homePrivate = $this->HomePrivate->get($id, [
    //         'contain' => []
    //     ]);

    //     $this->set('homePrivate', $homePrivate);
    // }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $homePrivate = $this->HomePrivate->newEntity();
    //     if ($this->request->is('post')) {
    //         $homePrivate = $this->HomePrivate->patchEntity($homePrivate, $this->request->getData());
    //         if ($this->HomePrivate->save($homePrivate)) {
    //             $this->Flash->success(__('The home private has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The home private could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('homePrivate'));
    // }

    /**
     * Edit method
     *
     * @param string|null $id Home Private id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function edit($id = null)
    // {
    //     $homePrivate = $this->HomePrivate->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $homePrivate = $this->HomePrivate->patchEntity($homePrivate, $this->request->getData());
    //         if ($this->HomePrivate->save($homePrivate)) {
    //             $this->Flash->success(__('The home private has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The home private could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('homePrivate'));
    // }

    /**
     * Delete method
     *
     * @param string|null $id Home Private id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $homePrivate = $this->HomePrivate->get($id);
    //     if ($this->HomePrivate->delete($homePrivate)) {
    //         $this->Flash->success(__('The home private has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The home private could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }
}
