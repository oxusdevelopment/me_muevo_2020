<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SelfRatedHealthTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SelfRatedHealthTable Test Case
 */
class SelfRatedHealthTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SelfRatedHealthTable
     */
    public $SelfRatedHealth;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SelfRatedHealth'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SelfRatedHealth') ? [] : ['className' => SelfRatedHealthTable::class];
        $this->SelfRatedHealth = TableRegistry::getTableLocator()->get('SelfRatedHealth', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SelfRatedHealth);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
