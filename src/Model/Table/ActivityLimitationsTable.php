<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ActivityLimitations Model
 *
 * @method \App\Model\Entity\ActivityLimitation get($primaryKey, $options = [])
 * @method \App\Model\Entity\ActivityLimitation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ActivityLimitation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ActivityLimitation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActivityLimitation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ActivityLimitation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ActivityLimitation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ActivityLimitation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ActivityLimitationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('activity_limitations');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('normal_activity')
            ->allowEmptyString('normal_activity');

        $validator
            ->integer('recreational_activity')
            ->allowEmptyString('recreational_activity');

        $validator
            ->integer('housework')
            ->allowEmptyString('housework');

        $validator
            ->integer('errand')
            ->allowEmptyString('errand');

        return $validator;
    }
}
