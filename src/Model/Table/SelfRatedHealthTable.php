<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * SelfRatedHealth Model
 *
 * @method \App\Model\Entity\SelfRatedHealth get($primaryKey, $options = [])
 * @method \App\Model\Entity\SelfRatedHealth newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\SelfRatedHealth[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\SelfRatedHealth|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SelfRatedHealth saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\SelfRatedHealth patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\SelfRatedHealth[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\SelfRatedHealth findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SelfRatedHealthTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('self_rated_health');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('general_point')
            ->allowEmptyString('general_point');

        return $validator;
    }
}
