<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Contenido2Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Contenido2Table Test Case
 */
class Contenido2TableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\Contenido2Table
     */
    public $Contenido2;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Contenido2'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Contenido2') ? [] : ['className' => Contenido2Table::class];
        $this->Contenido2 = TableRegistry::getTableLocator()->get('Contenido2', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Contenido2);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
