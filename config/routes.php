<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

use Cake\Core\Plugin;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::extensions(['pdf']);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.

    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    $routes->connect('/', ['controller' => 'Pages', 'action' => 'home']);
    $routes->connect('/advice', ['controller' => 'Pages', 'action' => 'advice']);
    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    // $routes->connect('/pages/*', ['controller' => 'Pages', 'action' => 'display']);

    $routes->connect('/contacto', ['controller' => 'Pages', 'action' => 'contacto']);
      
    $routes->connect('/resetPassword', ['controller' => 'Pages', 'action' => 'resetPassword']);

    $routes->connect('/registrar', ['controller' => 'Pages', 'action' => 'addUser']);

   
    
    // Formularios
    $routes->connect('/list_form', ['controller' => 'Pages', 'action' => 'listForm']);
    // $routes->connect('/users', ['controller' => 'Pages', 'action' => 'listForm']);

    $routes->connect('/logOut', ['controller' => 'Users', 'action' => 'logout']);

    // consejos
    $routes->connect('/how_to_ask', ['controller' => 'Pages', 'action' => 'howToAsk']);
    $routes->connect('/tips_to_prepare', ['controller' => 'Pages', 'action' => 'tipsToPrepare']);
    $routes->connect('/maintaining_good_communication',['controller'=>'Pages', 'action'=> 'maintainingGoodCommunication']);
    $routes->connect('/tips_for_remote_consultation',['controller'=>'Pages', 'action' => 'tipsForRemoteConsultation']);


    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *
     * ```
     * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
     * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
     * ```
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});


 
/**
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */

 /**
    * Load all plugin routes. See the Plugin documentation on
    * how to customize the loading of plugin routes.
**/
Plugin::routes();