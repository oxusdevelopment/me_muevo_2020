<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ArthritisSelftEfficacies Model
 *
 * @method \App\Model\Entity\ArthritisSelftEfficacy get($primaryKey, $options = [])
 * @method \App\Model\Entity\ArthritisSelftEfficacy newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ArthritisSelftEfficacy[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ArthritisSelftEfficacy|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArthritisSelftEfficacy saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ArthritisSelftEfficacy patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ArthritisSelftEfficacy[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ArthritisSelftEfficacy findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ArthritisSelftEfficaciesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('arthritis_selft_efficacies');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->integer('reduce_pain')
            ->allowEmptyString('reduce_pain');

        $validator
            ->integer('sleep_pain')
            ->allowEmptyString('sleep_pain');

        $validator
            ->integer('pain_interfere')
            ->allowEmptyString('pain_interfere');

        $validator
            ->integer('fequency_activity')
            ->allowEmptyString('fequency_activity');

        $validator
            ->integer('fatigue')
            ->allowEmptyString('fatigue');

        $validator
            ->integer('help_yourselft_sad')
            ->allowEmptyString('help_yourselft_sad');

        $validator
            ->integer('daily_pain')
            ->allowEmptyString('daily_pain');

        $validator
            ->integer('frustrations')
            ->allowEmptyString('frustrations');

        return $validator;
    }
}
