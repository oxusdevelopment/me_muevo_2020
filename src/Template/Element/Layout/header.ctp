<header>
	<div role="main" class="cuerpo">
		<div class="fondo-menu">
			<div class="contenedor">
				<?if(($this->request->params['controller'] != 'User')AND($this->request->params['action'] != 'login')) {?>
					<ul>
						<li class="header"><?=$this->Html->link('Iniciar Sesion', ['controller' => 'Pages', 'action' => 'listForm', 'style' => 'font-family:avenir;']);?></li>
					</ul>
				<?}?>
				<?if(($this->request->params['controller'] != 'usuario')and ($this->request->params['action'] != 'add')) {?>
					<ul>
						<li class="header"><?=$this->Html->link('Registrate', ['controller' => 'Pages', 'action' => 'addUser', 'style' => 'font-family:avenir;'])?></li>
					</ul>
				<?}?>
				<?if(($this->request->params['controller'] != 'pages') and ($this->request->params['action'] != 'home')){?>
					<ul>
						<li class="header"><a href="/" class="dropbtn" >Campaña</a></li>
					</ul>
				<?}?>
					<ul>
						<li class="header"><a href="/logOut" >Cerrar Sesión</a></li>
					</ul>
			
			</div>
		</div>
	</div>
</header>

<style>

	.open-hide {
		background-color: #f7f5ee;
		width: 35px;
		height: 35px;
		display: block;
		background-image: url(/img/boton-menu.png);
		background-repeat: no-repeat;
		background-position: center center;
		background-size: 25px;
		text-indent: -9999em;

		&:hover {
			cursor: pointer;
		}
	}

	.show {
		background-image: url(/img/boton-menu.png);
		background-size: 15px;


		+.smenu {
			height: 100px;
		}
	}
</style>