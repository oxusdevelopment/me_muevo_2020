<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Agregar Contenido
        <small>Preview page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Noticia</a></li>
        <li><a href="#"> Contenido</a></li>
        <li class="active">Agregar Contenido</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <?= $this->Form->create($contenido, ['type' => 'file']); ?>
        <div class="col-md-3 col-md-push-9 special-box">
            <div class="box box-purple">
                <div class="box-header with-border">
                    <h3 class="box-title">Navegación</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                        </button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-toolbar">
                                <?= $this->Form->button(__('<i class="fa fa-floppy-o fa-side"></i> Guardar'), ['class' => 'btn btn-default option']) ?>
                            </div>
                            <?php if (isset($noticia->id______con)) : ?>
                                <div class="btn-toolbar">
                                    <?= $this->Html->link(__('<i class="fa fa-eye fa-side"></i> Previsualizar'), [
                                        'action' => 'noticias',
                                        'controller' => 'previews', $noticia->id______con
                                    ], [
                                        'class' => 'btn btn-default option',
                                        'escape' => false,
                                        'target' => '_blank'
                                    ]) ?>
                                </div>
                            <?php endif; ?>
                            <div class="btn-toolbar">
                                <?= $this->Html->link(__('<i class="fa fa-repeat fa-side"></i> Volver'), ['action' => '/index'], ['class' => 'btn btn-default option', 'escape' => false]) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9 col-md-pull-3 special-box">
            <div class="box box-purple">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar Noticia</h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="form-group col-md-12" >
                            <?= $this->Form->control('Titulo', [
                                'label' => 'Título',
                                'class' => 'form-control',
                            ]);
                            ?>
                        </div>

                        <div class="form-group col-md-12">
                            <?= $this->Form->control('Cuerpo', [
                                'id' => 'cuerpo',
                                'label' => 'Cuerpo',
                                'class' => 'form-control',
                            ]);
                            ?>
                        </div>

                        <div class="form-group col-md-4">
                            <?= $this->Form->control('Estado_Contenido', [
                                'type' => 'select',
                                'label' => 'Estado',
                                'class' => 'form-control',
                                'options' => ['A' => 'Activo', 'I' => 'Inactivo'],
                            ]); ?>
                        </div>
                       
                        <div class="form-group col-md-12">
                        <h3>Subir imagen</h3>
                           <?= $this->Form->file('archivo'); ?>
                       </div>

                    </div>
                </div>

            </div>
        </div>

    <?= $this->Form->end() ?>
    </div>
</section>

<!-- // Probar para estilos <textarea ></textarea -->
   <!-- Main content -->
  <!--  <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">CK Editor
                <small>Advanced and full of features</small>
              </h3>
              <-- tools box -->
              <!-- <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div> --> 
              <!-- /. tools -->
            <!--/div-->
            <!-- /.box-header -->
            <!-- <div class="box-body pad">
              <form>
                    <textarea id="editor1" name="editor1" rows="10" cols="80">
                                            This is my textarea to be replaced with CKEditor.
                    </textarea>
              </form>
            </div>
          </div> -->
          <!-- /.box -->

          <!-- <div class="box">
            <div class="box-header">
              <h3 class="box-title">Bootstrap WYSIHTML5
                <small>Simple and fast</small>
              </h3> -->
              <!-- tools box -->
              <!-- <div class="pull-right box-tools">
                <button type="button" class="btn btn-default btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-default btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fa fa-times"></i></button>
              </div> -->
              <!-- /. tools -->
            <!--/div-->
            <!-- /.box-header -->
            <!-- <div class="box-body pad">
              
            <form>
                <textarea name= "Titulo" id="Titulo" value="titulo" class="textarea" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
              </form>
            </div>
          </div>
        </div> -->
        <!-- /.col-->
      <!--/div-->
      <!-- ./row -->
    <!--/section-->
    <!-- /.content -->
 <!-- bootstrap wysihtml5 - text editor -->
<?php echo $this->Html->css('AdminLTE./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min', ['block' => 'css']); ?>

<!-- CK Editor -->
<?php echo $this->Html->script('AdminLTE./bower_components/ckeditor/ckeditor', ['block' => 'script']); ?>
<!-- Bootstrap WYSIHTML5 -->
<?php echo $this->Html->script('AdminLTE./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min', ['block' => 'script']); ?>


<?php $this->start('scriptBottom'); ?>
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>
<?php $this->end(); ?>