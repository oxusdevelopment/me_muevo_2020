<?php
use Migrations\AbstractMigration;

class AlterTableCommunicationPhysiciansAddUserId extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('communication_physicians');

        $table->addColumn('user_id','integer',[
            'default' => 0,
            'null' => false
        ]);
        $table->update();
    }
}
