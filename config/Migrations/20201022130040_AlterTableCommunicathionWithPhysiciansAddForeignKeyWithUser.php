<?php
use Migrations\AbstractMigration;

class AlterTableCommunicathionWithPhysiciansAddForeignKeyWithUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('communication_physicians');
        $table->addForeignKey('user_id', 'users', 'id', array('delete' => 'NO_ACTION', 'update'=>'NO_ACTION'));
        $table->update();
    }
}
