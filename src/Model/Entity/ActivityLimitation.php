<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ActivityLimitation Entity
 *
 * @property int $id
 * @property int|null $normal_activity
 * @property int|null $recreational_activity
 * @property int|null $housework
 * @property int|null $errand
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 */
class ActivityLimitation extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
    ];
}
