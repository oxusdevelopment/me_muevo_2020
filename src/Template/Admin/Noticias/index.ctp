<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
      Noticias
        <small>Preview page</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Noticias</li>
      </ol>
    </section>
    
<section class="content">
<div class="row">
 <div class="col-md-3 col-md-push-9 special-box">
        
 </div>

    <div class="col-md-9 col-md-pull-3 special-box">
        <div class="box box-purple">
            <div class="box-header with-border">
                <h3 class="box-title">Listado</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover text-center">
                                <thead>
                                    <tr>
                                        <th>Título</th>
                                        <th>Cuerpo</th>
                                        <th>Fecha</th>                                        
                                        <th>Activa</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($files as $file): ?>
                                        <tr>
                                            <td><?= $file->Titulo ?></td>
                                            <td><?= $file->Cuerpo ?></td>
                                            <td><?= $file->Created ?></td>
                                          
                                            <td><?= $file->Estado_Contenido == 'A' ? '<i class=" fa fa-check"></i>' : "" ?></td>
                                            
                                            <td>
                                                <!--?= $this->Html->link('<i class="fa fa-eye"></i>', [
                                                        'action' => 'documents', $taxonomia->id______tax], [
                                                        'escape' => false , 'title' => 'Visualizar archivos',
                                                        'class' => 'btn btn-box-tool'
                                                    ]);? -->  

                                                <?= $this->Html->link('<i class="fa fa-edit"></i>', [
                                                        'action' => 'documents', $file->ID_Contenido], [
                                                        'escape' => false , 'title' => 'Editar',
                                                        'class' => 'btn btn-box-tool'
                                                    ]);
                                                ?>                                              
                                            </td>
                                            
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
           <div class="box-footer">
                <!--?= $this->element('Admin/Utils/paginator'); ?-->
            </div>
        </div>
    </div>
</div>

</section>