<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <style>
    ul{
        font-size: 15px;
    }
    h4{
        font-size: 18px;
        font-weight: bold;
    }
    .row{
        font-size: 12px;
        align-content: center;
    }
    li{
        border-bottom: 0px !important;
        border-top: 0px !important;
        border: 0px !important;
        float: none!important;
    }
   
 </style>
 
<!-- Page Content -->
<header class="masthead bg-primary text-white text-center">
	<img src="/img/header-landing.png" alt="" class="ilustracion5">
</header>
<div class="row">
    <div class="col-md-8 col-md-push-2">
            <li class="list-group-item" id="1">
                <h4 style="font-family: avenir;">¿Cómo hacer buenas preguntas en la consulta? <span style="font-size: 0.7em;"><a href="#0"></a></span></h4>
                <p style="font-family: avenir; font-size:15px !important;">
                    Hacer preguntas en la consulta nos va a ayudar a entender mejor lo que nos está
                    pasando y las acciones que debemos realizar a futuro. Pero muchas veces, en el
                    momento nos quedamos en blanco y al llegar a la casa vuelven a aparecer todas las
                    dudas.
                    Estos simples consejos pueden ayudarte a preparar tus preguntas:
                </p>
                    <li class="list-group-item" style="font-family: avenir; font-size:15px !important;"> 
                        Escribe una lista con las preocupaciones o preguntas para hacer a tu equipo de
                        salud (médico, enfermera, terapeuta, etc.).
                    </li>
                    <li class="list-group-item"style="font-family: avenir; font-size:15px !important;"> 
                        Marca o destaca las que tú consideras más importantes.
                    </li>
                    <li class="list-group-item"style="font-family: avenir; font-size:15px !important;"> 
                        Al inicio de la consulta, entrega tu listado al profesional de la salud. Esto es
                        clave porque lo más probable es que no pueda responderlas en su totalidad
                        por el tiempo acotado de la atención. Pero al mirarlas todas, podrá seleccionar
                        aquellas que mayor impacto pueden tener en tu salud.
                    </li>
                <p style="font-family: avenir; font-size:15px !important; padding: 10px;">
                    ¡No dejes de preparar tus preguntas para tu siguiente consulta!
                </p>
            </li>
        </ul>
    </div>
</div>

<div class="col">
<center>
    <h2 style="font-size:25px;  margin-right:3%; margin-left:3%; font-family: avenir;" >
        Evalúa el estado de tu enfermedad hoy para que puedas compartirlo con tu doctor.
    </h2>
    <br>
    <a href="/list_form"><button type="button" class="btn" style="width:120px; height:40px; font-size:15px; color: white;background-color:#d3562e;">Aquí</button></a>
</center>
</div>
