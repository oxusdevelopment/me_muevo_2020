<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * QuestionUsersFixture
 */
class QuestionUsersFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'disability_scale_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'activity_limitation_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'self_rated_health_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'communication_physician_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'arthritis_selft_efficacy_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'user_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'disability_scale_id' => ['type' => 'index', 'columns' => ['disability_scale_id'], 'length' => []],
            'activity_limitation_id' => ['type' => 'index', 'columns' => ['activity_limitation_id'], 'length' => []],
            'self_rated_health_id' => ['type' => 'index', 'columns' => ['self_rated_health_id'], 'length' => []],
            'communication_physician_id' => ['type' => 'index', 'columns' => ['communication_physician_id'], 'length' => []],
            'arthritis_selft_efficacy_id' => ['type' => 'index', 'columns' => ['arthritis_selft_efficacy_id'], 'length' => []],
            'user_id' => ['type' => 'index', 'columns' => ['user_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'question_users_ibfk_1' => ['type' => 'foreign', 'columns' => ['disability_scale_id'], 'references' => ['disability_scales', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'question_users_ibfk_2' => ['type' => 'foreign', 'columns' => ['activity_limitation_id'], 'references' => ['activity_limitations', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'question_users_ibfk_3' => ['type' => 'foreign', 'columns' => ['self_rated_health_id'], 'references' => ['self_rated_health', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'question_users_ibfk_4' => ['type' => 'foreign', 'columns' => ['communication_physician_id'], 'references' => ['communication_physicians', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'question_users_ibfk_5' => ['type' => 'foreign', 'columns' => ['arthritis_selft_efficacy_id'], 'references' => ['arthritis_selft_efficacies', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'question_users_ibfk_6' => ['type' => 'foreign', 'columns' => ['user_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'disability_scale_id' => 1,
                'activity_limitation_id' => 1,
                'self_rated_health_id' => 1,
                'communication_physician_id' => 1,
                'arthritis_selft_efficacy_id' => 1,
                'user_id' => 1,
                'modified' => '2020-10-09 19:01:21',
                'created' => '2020-10-09 19:01:21'
            ],
        ];
        parent::init();
    }
}
