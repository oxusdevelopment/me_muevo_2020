<section class="ilustracion">
    <div class="contenedor transparente">
        <div class="row">
            <div class="wrap">
                <div style="opacity:1 !important;">
                    <center>
                        <div class="login-box">
                            <h2>Recuperar contraseña</h2>
                            <div class="login-logo">
                                <b>Divorcio </b>En Linea
                            </div>
                            <div class="row">
                                <?= $this->Flash->render('auth') ?>
                                <?= $this->Flash->render() ?>
                            </div>
                            <br>
                            <?= $this->Form->create() ?>
                            <div class="login-box-body">
                                <div class="form-group2 has-feedback">
                                    <?= $this->Form->control('Clave', [
                                        'class' => 'form-control',
                                        'placeholder' => 'ingrese su nueva contraseña',
                                        'type' => 'password',
                                        'label' => false,
                                        'required' => true,
                                    ]) ?>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <div class="form-group2 has-feedback">
                                    <?= $this->Form->control('Clave2', [
                                        'class' => 'form-control',
                                        'placeholder' => 'ingrese nuevamente su contraseña nueva',
                                        'type' => 'password',
                                        'label' => false,
                                        'required' => true,
                                    ]) ?>
                                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                                </div>
                                <?= $this->Form->end() ?>
                                <div class="row">
                                    <div class="col-xs-4">
                                        <?= $this->Form->button('Guardar', [
                                            'class' => 'btn btn-primary btn-block btn-flat',
                                            'Controller' => 'Users', 'action' => 'NuevaContraseña',
                                        ])?>
                                    </div>
                                </div>
                                <?= $this->Form->end() ?>
                                <br>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>