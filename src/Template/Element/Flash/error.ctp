<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<section class="content-header">
    <div class="alert alert-danger alert-dismissible" onclick="this.classList.add('hidden');">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-ban"></i> <?= $message ?></h4>
        
    </div>
</section>