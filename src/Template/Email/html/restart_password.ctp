<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		.mail{
			width: 80%;
			margin: auto;
			border: 0.5px solid #E0E0E0 ;
		}

		header{
			height: 100px; 
			background-color:#E0E0E0; 
			width: 100%;
			display: flex;
			position: relative;
			justify-content: center;
		}
		.logo{
			width: 116px;
			height: 52px;
			margin: auto;
		}

		.content{
			height: auto; 
			width: 100%;
			color: #333333;
		}

		.title{
			text-align: center;
			font-size: 25px;
		}

		.content p{
			text-align: center; 
			font-size: 15px;
		}

		.link-box{
			height: 80px;
			width: 80%;
			margin: 30px auto;
			position: relative;
			display: flex;
		}

		.link{
			text-align: center;
			font-weight: bold;
			font-size: 15px;
			display: block;
			margin: auto;
			text-decoration: none;
		}

		footer{
			width: 100%; 
			height: 88px;
		}

		footer p{
			text-align: center; 
			font-size: 13px;
			color: #333333;
			font-weight: bold;
		}
		.multipleTariffs{
			width: 80%;
			margin: auto;
			border-collapse: collapse;
			border-spacing: 1px;
			text-align: center;
		}
		.multipleTariffs td, th{
			border: 1px solid #f4f4f4;
			padding: 10px;
		}
		.multipleTariffs thead tr th{
			background: #dff0d8;
		}
	</style>
</head>
<body>
	<div class="mail">
		<header>
			<img  class="logo" src="http://divorcio.oxus.cl/img/logo.png">
		</header>
		<div class="content">
			<h2 class="title">Estimado Usuario <?php echo $name;?></h2>
			<p>Se ha realizado una petición de cambio de contraseña a tu cuenta</p>
			<p>Tu link de restauración es</p>
			<div class="link-box">
				<?php echo '<a href="http://'.$_SERVER['SERVER_NAME'].'/Users/new-password/'.$email.'" class="link">Presione aquí</a>';?>
			</div>
		</div>
		<footer>
			<p>Te recordamos que este link solo es valido durante las siguientes 12 horas</p>
			<p>Si no realizaste esta solicitud o recibiste este correo por error porfavor ignoralo</p>
		</footer>
	</div>
</body>
</html>
