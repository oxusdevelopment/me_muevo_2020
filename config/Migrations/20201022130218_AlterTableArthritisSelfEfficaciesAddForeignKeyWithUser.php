<?php
use Migrations\AbstractMigration;

class AlterTableArthritisSelfEfficaciesAddForeignKeyWithUser extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('arthritis_selft_efficacies');
        $table->addForeignKey('user_id', 'users', 'id', array('delete' => 'NO_ACTION', 'update'=>'NO_ACTION'));
        $table->update();
    }
}
