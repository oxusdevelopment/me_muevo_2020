<section class="ilustracion_login">
    <div class="contenedor transparente">
        <div class="row">
            <div class="wrap">
                <div style="opacity:1 !important;">
                    <center>
                    <?= $this->Form->create() ?>
                    <div class="users form large-9 medium-8 columns content" >
        <?= $this->Form->create() ?>
        <fieldset>
        <center>
            <legend style="font-family:avenir;"><?= __('Registrese') ?></legend>
            <div style="margin-left:10%; margin-right:10%;">
            <?php
                echo $this->Form->control('name', 
                    [
                        'required',
                        'placeholder' => 'Ingrese su Nombre',
                        'style' => 'margin-left:-4%; font-family:avenir;',
                        'label' => 'Nombre'
                    ]
                );
                echo $this->Form->control('lastname', 
                    [
                        'required',
                        'placeholder' => 'Ingrese su Apellido',
                        'style' => 'margin-left:-4%; font-family:avenir;',
                        'label' => 'Apellido'
                    ]
                );
                echo $this->Form->control('email', 
                    [
                        'type' => 'email',
                        'placeholder' => 'Ingrese su Email',
                        'label' => 'Correo electrónico',
                        'style' => 'margin-left:-4%; font-family:avenir;',
                    ]
                );
                echo $this->Form->control('password',
                    [ 
                        'placeholder' => 'Ingrese su clave',
                        'required',
                        'type'=>'password',
                        'style' => 'margin-left:-4%; font-family:avenir;',
                        'label' => 'Contraseña'
                    ]
                );
            ?>
            </div>
        </center>
        </fieldset>
        <?= $this->Form->button(__('Aceptar')) ?>
        <?= $this->Form->end() ?>

        <h3 style="font-family:avenir;"> <?= $this->Html->link('Si ya estás registrado, ¡inicia sesión aquí!', ['controller' => 'Users', 'action' => 'login'])?></h1>
    <div class="row">
        <?= $this->Flash->render('auth') ?>
        <?= $this->Flash->render() ?>
    </div>
</div>  
</section>