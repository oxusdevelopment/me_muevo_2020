<?php
use Migrations\AbstractMigration;

class CreateTableArthritisSelftEfficacies extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('arthritis_selft_efficacies');

        $table->addColumn('reduce_pain', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('sleep_pain', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('pain_interfere', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('fequency_activity', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('fatigue', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('help_yourselft_sad', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('daily_pain', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('frustrations', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
