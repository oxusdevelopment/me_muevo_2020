<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Add User') ?></legend>
        <?php
            echo $this->Form->control('Nombre', ['required']);
            echo $this->Form->control('Apellido', ['required']);
            echo $this->Form->control('RUT', [ 'required']);
            echo $this->Form->control('Telefono', ['required']);
            echo $this->Form->control('Email', ['required', 'type' => 'email']);
            echo $this->Form->control('Nombre_Usuario', ['required']);
            echo $this->Form->control('Clave',[ 'required','type'=>'password']);
            echo $this->Form->control('Rol', [
                'empty' => 'Seleccione',
                'options' => ['1' => 'Administrador', '2' => 'Usuario'],
                'type' => 'select',
                'required'
            ]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Aceptar')) ?>
    <?= $this->Form->end() ?>
</div>  
