<div class="col-md-12">
    <div class="container-fluid">
        <div class="row" style="margin-left:700px;">
            <? $creationDate = date("d-m-Y");
                print_r($creationDate);?>
        </div>
        <div class="row">
            <img style="width:100px;" src=<?=WWW_ROOT."/img/header.png"?> alt="">
            <img style="width:100px;" src=<?=WWW_ROOT."/img/LOGO_VOLAR.jpg"?> alt="">
        </div>
        <div class="container">
            <center><h1>Informe</h1></center>
            <center><h1>Evaluaciones autoreportadas por paciente</h1></center>

        </div>
        <div class="col-md-10 col-md-push-1">
            <h2>Datos del Paciente</h2>
        </div>
        <div class="col-md-12">
            <div class="col-md-6">
               <li>
                    <strong>Nombre: </strong><?=$user->name;?>
               </li>
               <li>
                    <strong>Apellido: </strong><?=$user->lastname;?>
               </li>
               <li>
                   <strong>RUT: </strong><?=$user->rut;?>
               </li>
               <li>
                   <strong>Fecha de nacimiento: </strong><?=$user->birth_date?>
               </li>
               <li>
                   <strong>E-mail: </strong><?=$user->email?>
               </li>
            </div>
            <div class="col-md-6">
                <li>
                    <strong>Sexo: </strong> <?if($user->sex == 1){print_r('Femenino');}else if($user->sex == 2){print_r('Masculino');}else{print_r('No informado');}?>
                </li>
                <li>
                    <strong>Previsión: </strong><?if($user->healt == 1){print_r('Fonasa');}else if($user->healt == 2){print_r('Isapre');}else{print_r('No Informado');}?>
                </li>
                <li>
                    <strong>Teléfono: </strong><?=$user->phone;?>
                </li>
                <li>
                    <strong>Fecha de detección de la enfermedad: </strong> <?=$user->detection;?>
                </li>
            </div>
            <div class="col-md-12">
            <div class="container">
                <h1 style="margin-left:6%;">Cuestionarios:</h1>
                <h2>Salud autopercibida:</h2>
            </div>
            <div class="col-md-12">
                <!--?foreach($self_rated_healt as $rated_healt){?--> 
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr class="success">
                                <th></th>
                                <th class="rotate" style="text-align:center;"><div><strong>Respuesta Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <th class="row-header" style="text-align:left;"><strong>Generalmente, ¿Ud. diría que su salud es ... ?</strong></th>
                                <td style="text-align:center;"><?=isset($self_rated_healt->general_point)?$self_rated_healt->general_point:''?></td>
                            </tr>
                        </tbody>
                    </table>  
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>Resultado Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr style="text-align:center;">
                                <td><?=(isset($self_rated_healt->general_point)?$self_rated_healt->general_point:'')/1?></td>
                            </tr>
                        </tbody>
                        <p><strong>Salud Autopercibida:</strong> la escala mide XXXX , medida a través de la escala " Spanish Self-Rated Health” desarrollada por Stanford Patient Education Research Center.  Consta de 1 ítem y su puntaje varía entre 1 y 5, con una media de 4,04.  Un mayor puntaje en la escala se traduce en peor estado de salud autopercibido.</p>
                    </table>  
                <!--?}?-->
            </div>
            <div class="container">
                <h2>Valoracion del dolor: </h2>
            </div>
            <div class="col-md-12">
                <table class="table table-header-rotated" style="font-size: 12px;">
                    <thead>
                        <tr class="success">
                            <th class="rotate" style="text-align:center;"><div><strong>Respuesta Paciente</strong></div></th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                            <td style="text-align:center;"><?=isset($pain->pain_score)?$pain->pain_score:''?></td>
                        </tr>
                    </tbody>
                </table>  
                <table class="table table-header-rotated" style="font-size: 12px;">
                    <thead>
                        <tr  class="success">
                            <!-- <th class="rotate"><div><strong>Patient Result</strong></div></th> -->
                        </tr> 
                    </thead>
                    <tbody>
                        <tr style="text-align:center;">
                            <!-- <td>1</td> -->
                        </tr>
                    </tbody>
                    <p><strong>Valoración del dolor:</strong> la escala mide XXXX , medida a través de la escala "Spanish Pain Visual Numeric” desarrollada por Stanford Patient Education Research Center.  Consta de 1 ítem y su puntaje varía entre 0 y 10, con una media de 4,26.  Un mayor puntaje en la escala se traduce en XXXX.</p>
                </table>  
            </div>

                <div class="container">
                    <h2>Capacidad Funcional:</h2>
                </div>
                <div class="col-md-12">
                    <!--?foreach($disability as $disability){?-->
                        <table class="table table-header-rotated" style="font-size: 12px;">
                            <thead>
                                <tr  class="success">
                                    <th><strong>¿Actualmente puede Ud:</strong></th>
                                    <th class="rotate"><div><strong>Respuesta paciente</strong></div></th>
                                </tr> 
                            </thead>
                            <tbody>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>1.- Vestirse, incluyendo amarrarse los zapatos y abrocharse(abotonarse)?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->dress)?$disability->dress:''?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>2.- Acostarse y levantarse de la cama?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->get_up)?$disability->get_up:''?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>3.-Levantar hasta su boca una taza o vaso lleno?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->lifting_cup)?$disability->lifting_cup:''?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>4.- Caminar al aire libre en terreno plano?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->walking)?$disability->walking:''?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>5.- Bañarse y secarse todo el cuerpo?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->swim)?$disability->swim:''?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>6.- Agacharse para recoger ropa del piso?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->lifting_clothin)?$disability->lifting_clothing:''?></td>

                                </tr>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>7.- Abrir y cerrar las llaves de agua (los grifos)?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->open_tap)?$disability->open_tap:''?></td>
                                </tr>
                                <tr>
                                    <th class="row-header"style="text-align:left;"><strong>8.- Subir y bajar del auto (carro)?</strong></th>
                                    <td style="text-align:center;"><?=isset($disability->get_up_car)?$disability->get_up_car:''?></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <table class="table table-header-rotated" style="font-size: 12px;">
                            <thead>
                                <tr  class="success">
                                    <th class="rotate"><div><strong>Resultado Paciente</strong></div></th>
                                </tr> 
                            </thead>
                            <tbody>
                                <tr style="text-align:center;">
                                    <td><?=(isset($disability->dress)?$disability->dress:''+isset($disability->get_up)?$disability->get_up:''+isset($disability->lifting_cup)?$disability->lifting_cup:''+isset($disability->walking)?$disability->walking:''+isset($disability->swim)?$disability->swim:''+isset($disability->lifting_clothing)?$disability->lifting_clothing:''+isset($disability->open_tap)?$disability->open_tap:''+isset($disability->get_up_car)?$disability->get_up_car:'')/8?></td>
                                </tr>
                            </tbody>
                            <p><strong>Capacidad Funcional:</strong> la escala mide XXXX, medida a través de la escala “Spanish Stanford HAQ 8-Item Disability Scale” desarrollada por Stanford Patient Education Research Center.  Condta de 8 ítems y su puntaje varía entre 0 y 3, con una media de 1,7.  Un mayor puntaje en la escala se traduce eb XXXX. </p>
                        </table>
                    <!--?}?-->
                </div>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <div class="container">
                <h2>Percepcion de autoeficacia:</h2>
            </div>
            <div class="col-md-12">
                <!--?foreach($arthritis as $arthri){?-->
                    <table class="table table-header-rotated" style="font-size: 10px;">
                        <thead>
                            <tr class="success">
                                <th></th>
                                <th class="rotate"><div><strong>Respuesta Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>1.- ¿Qué tan seguro se siente Ud. de poder reducir bastante su dolor?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->reduce_pain)?$arthritis->reduce_pain:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>2.- ¿Qué tan seguro se siente Ud. de poder evitar que el dolor de la artritis interfiera con su sueño?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->sleep_pain)?$arthritis->sleep_pain:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>3.-¿Qué tan seguro se siente Ud. de poder evitar que el dolor de la artritis interfiera con las cosas que quiere hacer?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->pain_interfere)?$arthritis->pain_interfere:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>4.- ¿Qué tan seguro se siente Ud. de poder regular su actividad para mantenerse activo sin empeorar (agravar) su artritis?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->fequency_activity)?$arthritis->fequency_activity:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>5.- ¿Qué tan seguro se siente Ud. de poder evitar que la fatiga (el cansancio), debido a su artritis, interfiera con las cosas que quiere hacer?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->fatigue)?$arthritis->fatigue:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>6.- ¿Qué tan seguro se siente Ud. de poder ayudarse a sí mismo a sentirse mejor si se siente triste?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->help_yourselft_sad)?$arthritis->help_yourselft_sad:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>7.- Comparándose con otras personas con artritis como la suya, ¿qué tan seguro se siente Ud. de poder sobrellevar el dolor de artritis durante sus actividades diarias?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->daily_pain)?$arthritis->daily_pain:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>8.- ¿Qué tan seguro se siente Ud. de poder sobrellevar la frustración debido a su artritis?</strong></th>
                                <td style="text-align:center;"><?=isset($arthritis->frustrations)?$arthritis->frustrations:''?></td>
                            </tr>
                        </tbody>
                    </table>  
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>Resultado Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr style="text-align:center;">
                                <td><?=isset($arthritis->reduce_pain)?($arthritis->reduce_pain+$arthritis->sleep_pain+$arthritis->pain_interfere+$arthritis->fequency_activity+$arthritis->fatigue+$arthritis->help_yourselft_sad+$arthritis->daily_pain+$arthritis->frustrations)/8:'0'?></td>
                            </tr>
                        </tbody>
                        <p><strong>Percepción de auto eficacia:</strong> la escala mide el grado de autoeficacia percibida para manejar su artritis, medida a través de la escala "Spanish Arthritis Self-Efficacy" desarrollada por Stanford Patient Education Research Center.  Consta de 8 ítems y su puntaje varía entre 1 y 10, con una media de 5,9.  Un mayor puntaje en la escala se traduce en un mayor sentido de autoeficacia.</p>
                    </table>   
                <!--?}?-->
            </div>
        </div>
        <div class="col-md-12">
            <div class="container">
                <h2>Percepción de salud social:</h2>
            </div>
            <div class="col-md-12">
                <!--?foreach($activity_limitation as $activity){?-->
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr>
                                <th></th>
                                <th class="rotate" style="text-align:left;"><div><strong>Respuesta Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr>
                            <th class="row-header"style="text-align:left;"><strong>1.- En sus actividades normales con sus familiares, amigos, vecinos o grupos:</strong></th>
                                <td style="text-align:center;"><?=isset($activity_limitation->normal_activity)?$activity_limitation->normal_activity:''?></td>
                            </tr>
                            <tr>
                            <th class="row-header"style="text-align:left;"><strong>2.- En sus actividades recreativas o pasatiempos:</strong></th>
                                <td style="text-align:center;"><?=isset($activity_limitation->recreational_activity)?$activity_limitation->recreational_activity:''?></td>
                            </tr>
                            <tr>
                                <th class="row-header"style="text-align:left;"><strong>3.- En sus que haceres domésticos (tareas del hogar):</strong></th>
                                <td style="text-align:center;"><?=isset($activity_limitation->housework)?$activity_limitation->housework:''?></td>
                            </tr>
                            <tr>
                            <th class="row-header"style="text-align:left;"><strong>4.- En sus mandados / recados y compras:</strong></th>
                                <td style="text-align:center;"><?=isset($activity_limitation->errand)?$activity_limitation->errand:''?></td>
                            </tr>
                        </tbody>
                    </table>  
                    <br>
                    <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>Resultado Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr style="text-align:center;">
                                <td><?=isset($activity_limitation->normal_activity)?($activity_limitation->normal_activity+$activity_limitation->recreational_activity+$activity_limitation->housework+$activity_limitation->errand)/4:''?></td>
                            </tr>
                        </tbody>
                        <p><strong>Percepción de salud social: </strong> la escala mide el grado de interferencia de la enfermedad en sus actividades cotidianas, medida a través de la escala "Spanish Social/Role Activities Limitations Scale” desarrollada por Stanford Patient Education Research Center.  Consta de 4 ítems y su puntaje varía entre 0 y 4, con una media de 1,08.  Un mayor puntaje en la escala se traduce en mayor limitación en las actividades cotidianas.</p>
                    </table>  
                <!--?}?-->
            </div>
           

            <div class="container">
                <h2>Comunicación con equipos de salud:</h2>
            </div>
            <div class="col-md-12">
                <!--?foreach($communication as $commun){?-->
                <table class="table table-header-rotated" style="font-size: 10px;">
                    <thead>
                        <tr class="success">
                            <th></th>
                            <th class="rotate"><div><strong>Respuesta Paciente</strong></div></th>
                        </tr> 
                    </thead>
                    <tbody>
                        <tr>
                            <th class="row-header" style="text-align:left;"><strong>1.- Prepara una lista de preguntas para su médico:</strong></th>
                            <td style="text-align:center;"><?=isset($communication->medial_question)?$communication->medial_question:''?></td>
                        </tr>
                        <tr>
                            <th class="row-header" style="text-align:left;"><strong>2.- Hace preguntas acerca de las cosas que quiere saber y de las cosas que no entiende acerca de su tratamiento</strong></th>
                            <td style="text-align:center;"><?=isset($communication->make_question)?$communication->make_question:''?></td>
                        </tr>
                        <tr>
                            <th class="row-header" style="text-align:left;"><strong>3.- Habla sobre algún problema personal que puede estar relacionado con su enfermedad</strong></th>
                            <td style="text-align:center;"><?=isset($communication->personal_problem)?$communication->personal_problem:''?></td>
                        </tr>
                    </tbody>
                </table> 
                <br>
                <table class="table table-header-rotated" style="font-size: 12px;">
                        <thead>
                            <tr  class="success">
                                <th class="rotate"><div><strong>Resultado Paciente</strong></div></th>
                            </tr> 
                        </thead>
                        <tbody>
                            <tr style="text-align:center;">
                                <td><?=isset($communication->medial_question)?($communication->medial_question+$communication->make_question+$communication->personal_problem)/3:''?></td>
                            </tr>
                        </tbody>
                        <p><strong>Comunicación con el equipo de salud: </strong>la escala mide XXXX, medida a través de la escala “Spanish Communication with Physicians” desarrollada por Stanford Patient Education Research Center.  Consta de 3 ítems y su puntaje varía entre 0 y 5, con una media de 1,64.  Un mayor puntaje en la escala se traduce en una mejor comunicación con el equipo de salud.</p>
                    </table>  
                <!--?}?-->  
            </div>
          
        </div>
    </div>
</div>