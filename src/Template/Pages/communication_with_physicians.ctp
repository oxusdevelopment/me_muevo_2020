
<div class="row">
    <div class="col-md-12">
        <h3 class="title">Comunicación con equipos de salud</h3>
        <div class="box-body">
            <div class="row">
                <div class="col-md-10 col-md-push-1" >
                    <?= $this->Form->create(); ?>
                    <table class="table table-header-rotated" style="font-size: 10px;">
                                <thead>
                                    <tr class="success">
                                        <th></th>
                                        <th class="rotate"><div><strong>Nunca</strong></div></th>
                                        <th class="rotate"><div><strong>Casi nunca</strong></div></th>
                                        <th class="rotate"><div><strong>Algunas veces</strong></div></th>
                                        <th class="rotate"><div><strong>Con alguna frecuencia</strong></div></th>
                                        <th class="rotate"><strong>Muy frecuente</strong></th>
                                        <th class="rotate"><strong>Siempre</strong></th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <p><strong>Cuándo tiene consulta con su médico, ¿con qué frecuencia Ud. hace lo siguiente?:</strong></p>
                                    <tr>
                                        <th class="row-header"><strong>1.- Prepara una lista de preguntas para su médico:</strong></th>
                                        <td><input type="radio" value='0' name='medial_question' required></td>
                                        <td><input type="radio" value='1' name='medial_question' required></td>
                                        <td><input type="radio" value='2' name='medial_question' required></td>
                                        <td><input type="radio" value='3' name='medial_question' required></td>
                                        <td><input type="radio" value='4' name='medial_question' required></td>
                                        <td><input type="radio" value='5' name='medial_question' required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>2.- Hace preguntas acerca de las cosas que quiere saber y de las cosas que no entiende acerca de su tratamiento</strong></th>
                                        <td><input type="radio" value='0' name='make_question' required></td>
                                        <td><input type="radio" value='1' name='make_question' required></td>
                                        <td><input type="radio" value='2' name='make_question' required></td>
                                        <td><input type="radio" value='3' name='make_question' required></td>
                                        <td><input type="radio" value='4' name='make_question' required></td>
                                        <td><input type="radio" value='5' name='make_question' required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>3.- Habla sobre algún problema personal que puede estar relacionado con su enfermedad</strong></th>
                                        <td><input type="radio" value='0' name='personal_problem' required></td>
                                        <td><input type="radio" value='1' name='personal_problem' required></td>
                                        <td><input type="radio" value='2' name='personal_problem' required></td>
                                        <td><input type="radio" value='3' name='personal_problem' required></td>
                                        <td><input type="radio" value='4' name='personal_problem' required></td>
                                        <td><input type="radio" value='5' name='personal_problem' required></td>
                                    </tr>
                                </tbody>
                            </table>   
                       
                            <?= $this->Html->link('Volver',
                                ['controller' => 'Pages', 'action' => 'list_form'],
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                            <?= $this->Form->button('Guardar', ['class' => 'btn btn-md btn-danger option','id' => 'save'],['escape'=>false]); ?>
                    
                    <?= $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
</div>
