<style>
.list-group-item:last-child {
    margin-bottom: 0;
    border-bottom-right-radius: .25rem;
    border-bottom-left-radius: .25rem;
}
.list-group-item {
    position: relative !important;
    display: block !important;
    padding: .75rem 1.25rem !important;
    background-color: #fff !important;
}
li {
    border-bottom: 0px !important;
    border-top: 0px !important;
    border: 0px !important;
    float: none!important;
}
</style>
<footer class="main-footer">
	<div class="col-md-12">
		<div class="col-md-6">
			<br>
			<center>
			<a href="javascript:void(0)" title="Fundación me muevo"><img src="/img/header.png" alt="" style="" class=""></a>	
			br
			<li class="list-group-item" style="color:black;">
				<ol>
                    <li class="list-group-item" style="font-family:avenir">Fundación Me Muevo 2014.</li>
					<li class="list-group-item" style="font-family:avenir">RUT: 65.088.095-1</li>
					<li class="list-group-item" style="font-family:avenir">Telefono: +56987433259</li>
					<li class="list-group-item" style="font-family:avenir">Providencia, Santiago de Chile.</li>
					<li class="list-group-item" style="font-family:avenir">comunicaciones@memuevo.cl</li>
				</ol>
			</li>
			</center>

		</div>
		<div class="col-md-6">
			<br>
			<center>
			<a href="javascript:void(0)" title="Fundación volar"><img src="/img/LOGO_VOLAR.jpg" alt="" class=""></a>
			<br>
			<li class="list-group-item" style="color:black;">
				<ol>
                    <li class="list-group-item" style="font-family:avenir">Corporacion Pro Ayuda al Enfermo Reumático VOLAR</li>
					<li class="list-group-item" style="font-family:avenir">www.volarchile.cl | volar@volarchile.cl | @volarchile</li>
					<li class="list-group-item" style="font-family:avenir">Email: volar@volarchile.cl|</li>
					<li class="list-group-item" style="font-family:avenir">ar.leyricartesoto@volarchile.cl</li>
					<li class="list-group-item" style="font-family:avenir">Providencia, Santiago de Chile</li>
					<li class="list-group-item" style="font-family:avenir">comunicaciones@memuevo.cl</li>
				</ol>
			</li>
			</center>
		</div>
	</div>
	<div class="col-md-12">
		<center>
			<p style="color:black;" style="font-family:avenir">Transparencia: Creador de campaña: Brand Factory, OXUS.cl. Financiamiento: Esta campaña se gesta gracias al apoyo desinteresado de la industria, Patrocinio de Colegio Médico y Agrupaciones de pacientes.</p>
		</center>
	</div>
</footer>
