<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * Contenido2Fixture
 */
class Contenido2Fixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'contenido2';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'ID_Contenido' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'Titulo' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'Cuerpo' => ['type' => 'string', 'length' => 45, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'Created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'Modified' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'Enlace_URL' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'Ruta_Archivo' => ['type' => 'string', 'length' => 45, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'Tipo_Archivo' => ['type' => 'string', 'length' => 12, 'null' => true, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['ID_Contenido'], 'length' => []],
            'ID_Categoria' => ['type' => 'foreign', 'columns' => ['ID_Contenido'], 'references' => ['Categoria', 'ID_Categoria'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'ID_Estado' => ['type' => 'foreign', 'columns' => ['ID_Contenido'], 'references' => ['Estado', 'ID_Estado'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'ID_Noticia_Categoria' => ['type' => 'foreign', 'columns' => ['ID_Contenido'], 'references' => ['Noticia_Categoria', 'ID_Noticia_Categoria'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'ID_Usuario' => ['type' => 'foreign', 'columns' => ['ID_Contenido'], 'references' => ['Usuario', 'ID_Usuario'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'ID_Contenido' => 1,
                'Titulo' => 'Lorem ipsum dolor sit amet',
                'Cuerpo' => 'Lorem ipsum dolor sit amet',
                'Created' => '2019-04-10 14:06:38',
                'Modified' => '2019-04-10 14:06:38',
                'Enlace_URL' => 'Lorem ipsum dolor sit amet',
                'Ruta_Archivo' => 'Lorem ipsum dolor sit amet',
                'Tipo_Archivo' => 'Lorem ipsu'
            ],
        ];
        parent::init();
    }
}
