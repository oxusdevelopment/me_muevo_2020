<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <style>
    ul{
        font-size: 15px;
    }
    h4{
        font-size: 18px;
        font-weight: bold;
    }
    .row{
        font-size: 12px;
        align-content: center;
    }
    li{
        border-bottom: 0px !important;
        border-top: 0px !important;
        border: 0px !important;
        float: none!important;
    }
   
 </style>
 
<!-- Page Content -->
<header class="masthead bg-primary text-white text-center">
	<img src="/img/header-landing.png" alt="" class="ilustracion5">
</header>
<div class="row">
    <div class="col-md-8 col-md-push-2">
        <li class="list-group-item" id="1">
            <h4 style="font-family: avenir;">Consejos para monitorear tus síntomas <span style="font-size: 0.7em;"><a href="#0"></a></span></h4>
            <p style="font-family: avenir; font-size:15px !important;">
                Una buena práctica que llevan a cabo las personas activas en el manejo de su
                condición de salud es el monitoreo de sus síntomas entre cada consulta. De esta
                forma pueden reportar con detalle los cambios que puedan haber ocurrido en ese
                tiempo.
                Para esto, es bueno que puedas tener una libreta o cuaderno donde vayas escribiendo
                tus reflexiones cada cierto tiempo. Algunas preguntas interesantes de hacerse son:
            </P>
            <li class="list-group-item" style="font-family: avenir; font-size:15px !important;">
                Tus síntomas en términos generales ¿están mejor, peor o igual?
            </li>
            <li class="list-group-item" style="font-family: avenir; font-size:15px !important;"> 
                Si has tenido cambios en tus síntomas (es decir, si han aumentado, disminuido
                o cambiado), estos cambios ¿han sido lentos o rápidos?
            </li>
            <li class="list-group-item" style="font-family: avenir; font-size:15px !important;"> 
                ¿Ha cambiado o ha pasado algo en tu vida que te pueda estar afectando?
            </li>
            <li class="list-group-item" style="font-family: avenir; font-size:15px !important;"> 
                No olvides escribir sobre las cosas que haces para ayudarte a mejorar tus
                síntomas.
            </li>
            <p style="font-family: avenir; font-size:15px !important; padding: 12px;"> 
                Y recuerda: no es necesario registrar esta información todos los días, tampoco
                queremos vivir sólo ocupados de nuestra enfermedad. Pero sí es bueno escribir cada
                cierto tiempo o cuando ocurran cambios importantes.
                Estas reflexiones puedes compartirlas cuando vayas a control con tu equipo de salud.
                Así podrán saber más de ti durante todo el tiempo en que no se vieron.
            </p>
        </li>
    </div>
</div>

<div class="col">
<center>
    <h2 style="font-size:25px;  margin-right:3%; margin-left:3%; font-family: avenir;" >
        Evalúa el estado de tu enfermedad hoy para que puedas compartirlo con tu doctor.
    </h2>
    <br>
    <a href="/list_form"><button type="button" class="btn" style="width:120px; height:40px; font-size:15px; color: white;background-color:#d3562e;">Aquí</button></a>
</center>
</div>
