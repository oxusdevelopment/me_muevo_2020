<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\GroupsTable|\Cake\ORM\Association\BelongsTo $Groups
 * @property \App\Model\Table\ActivityLimitationsTable|\Cake\ORM\Association\HasMany $ActivityLimitations
 * @property \App\Model\Table\ArthritisSelftEfficaciesTable|\Cake\ORM\Association\HasMany $ArthritisSelftEfficacies
 * @property \App\Model\Table\CommunicationPhysiciansTable|\Cake\ORM\Association\HasMany $CommunicationPhysicians
 * @property \App\Model\Table\DisabilityScalesTable|\Cake\ORM\Association\HasMany $DisabilityScales
 * @property \App\Model\Table\QuestionUsersTable|\Cake\ORM\Association\HasMany $QuestionUsers
 * @property \App\Model\Table\SelfRatedHealthTable|\Cake\ORM\Association\HasMany $SelfRatedHealth
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Groups', [
            'foreignKey' => 'group_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ActivityLimitations', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('ArthritisSelftEfficacies', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('CommunicationPhysicians', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('DisabilityScales', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('QuestionUsers', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('SelfRatedHealth', [
            'foreignKey' => 'user_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->allowEmptyString('email', false);

        $validator
            ->scalar('password')
            ->maxLength('password', 250)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false);

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->allowEmptyString('name', false);

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 255)
            ->requirePresence('lastname', 'create')
            ->allowEmptyString('lastname', false);

        $validator
            ->scalar('rut')
            ->maxLength('rut', 255)
            ->allowEmptyString('rut');

        $validator
            ->scalar('birth_date')
            ->maxLength('birth_date', 255)
            ->allowEmptyString('birth_date');

        $validator
            ->integer('sex')
            ->allowEmptyString('sex');

        $validator
            ->integer('healt')
            ->allowEmptyString('healt');

        $validator
            ->integer('phone')
            ->allowEmptyString('phone');

        $validator
            ->scalar('detection')
            ->maxLength('detection', 255)
            ->allowEmptyString('detection');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['group_id'], 'Groups'));

        return $rules;
    }
}
