<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h3 class="title">Salud Autopercibida</h3>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10 col-md-push-1" >
                        <?= $this->Form->create(); ?>
                            <table class="table table-header-rotated" style="font-size: 12px;">
                                <thead>
                                    <tr class="success">
                                        <th></th>
                                        <th class="rotate"><div><strong>Excelente</strong></div></th>
                                        <th class="rotate"><div><strong>Muy buena</strong></div></th>
                                        <th class="rotate"><div><strong>Buena</strong></div></th>
                                        <th class="rotate"><div><strong>Regular</strong></div></th>
                                        <th class="rotate"><strong>Mala</strong></th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <p><strong>Durante la última semana, ¿cuánto ha interferido su salud en lo siguiente?</strong></p>
                                    <tr>
                                        <th class="row-header"><strong>Generalmente, ¿Ud. diría que su salud es ... ?</strong></th>
                                        <td><input type="radio" value='1' name='general_point' required></td>
                                        <td><input type="radio" value='2' name='general_point' required></td>
                                        <td><input type="radio" value='3' name='general_point' required></td>
                                        <td><input type="radio" value='4' name='general_point' required></td>
                                        <td><input type="radio" value='5' name='general_point' required></td>
                                    </tr>
                                </tbody>
                            </table>        
                                <?= $this->Html->link('Volver',
                                    ['controller' => 'Pages', 'action' => 'list_form'],
                                    ['class' => 'btn btn-danger option', 'escape' => false]) 
                                ?>
                                <?= $this->Form->button('Guardar', ['class' => 'btn btn-md btn-danger option','id' => 'save'],['escape'=>false]); ?>
                        <?= $this->Form->end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>