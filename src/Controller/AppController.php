<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\MailerAwareTrait;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    private $emailContacto = 'hugo.hjpb@gmail.com';
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Flash');
        $this->loadComponent('Cookie', ['expires' => '1 year']);

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');

        $this->loadComponent('Auth', [
            'authorize' => [
                'Controller'
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ]
                ]
            ],
            'authError' => 'Acceso denegado.',
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'loginRedirect'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'unauthorizedRedirect' => $this->referer()
            //'unauthorizedRedirect' => $this->referer() // Si no está autorizado,
            //el usuario regresa a la página que estaba
        ]);

        // Permite ejecutar la acción display para que nuestros controladores de páginas
        // sigan funcionando.
        $this->Auth->allow(['login','loginRedirect']);

    }


    public function beforeFilter(Event $event)
    {

        if ($this->Auth->user() !== null){
            $this->set('currentUser', $this->Auth->user());
            $this->currentUser = $this->Auth->user();
        }

        $this->Auth->allow([
            'home', 'default', 'beneficios', 'procesos',
            'preguntas', 'noticias', 'categorias', 'quienes', 'contacto', 'tarifas',
            'preguntasfrecuentes', 'recuperar', 'nuevaContraseña', 'newPassword',
            'resetPassword','detallenoticia','Content', 'Form', 'Fields','detalles', 'addUser', 
            'home_private', 'advice', 'howToAsk', 'tipsToPrepare', 'maintainingGoodCommunication', 
            'tipsForRemoteConsultation'
        ]);
    }

    public function isAuthorized($user)
    {
        // Admin can access every action
        //if (isset($user['Nombre_Rol']) && $user['Nombre_Rol'] === 'Admin') {
        return true;
        //}

        // Default deny
        //return false;
    }

    public function beforeRender(Event $event)
    {
        $array_controllers = [
            'Home',
            'Procesos',
            'Beneficios',
            'Preguntas',
            'Noticias',
            'Categorias',
            'Dashboard',
            'Quienes', 
            'Users',
            'Tarifas',
            'Solicitudes',
            'Form',
            'Content',
            'Fields'
             
        ];
        if (isset($this->request->params['controller']) && in_array($this->request->params['controller'], $array_controllers) 
        && $this->request->params['action'] != 'login' 
        && $this->request->params['action'] != 'newPassword' 
        && $this->request->params['action'] != 'resetPassword'
        && $this->request->params['action'] != 'content' 
        && $this->request->params['action'] != 'Form' 
        && $this->request->params['action'] != 'Fields' 
        && $this->request->params['action'] != 'recuperar'){
        $this->viewBuilder()->setLayout('admin');
           
        }
    }

    protected function isLoggedIn()
    {
        return ($this->currentUser !== null);
    }

    protected function emailContacto(){
        return $this->emailContacto;
    }
    
    use MailerAwareTrait;
    protected function sendNotification($to, $template, $viewVars, $subject, $attachments = null, $bcc = false){
        /*
            $to = A quién o quienes se les enviará el correo.
            $template = Archivo con diseño del correo. Debe estar ubicado en src/Template/Email/html y debe tener la extensión ctp.
            $profile = Configuración que se utilizará para enviar el correo. Éstas configuraciones se encuentran en el archivo app.php
            $viewVars = Array de variables que serán usadas en el Template.
            $subject = Asunto del correo.
            $attachments = Archivo(s) que será(n) adjuntado(s) en el correo. Los archivos deben estar en el servidor y se debe proporcionar la ruta completa al archivo.
                Ej: /var/www/ucclog/webroot/img/example.jpg.
            $bcc = A quién o quienes se les enviará el correo de forma oculta.
        */

        /*Vista por defecto para correo recibe 2 variables: title => Título del correo, message => Cuerpo del correo*/

        $args = new \stdClass();

        $args->to = $to;//['victor.adasme@oxus.cl', 'isaac.jara@oxus.cl', 'soporte@oxus.cl'];

        $args->template = $template;
        $args->layout = 'default';
        $args->viewVars = $viewVars;
        $args->subject = $subject;
        $args->attachments = $attachments;
        $this->getMailer('Default')->send('mail', [$args]);
        
    }

}