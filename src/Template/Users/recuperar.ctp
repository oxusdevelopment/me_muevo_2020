<section class="ilustracion_login">
    <div class="contenedor transparente">
        <div class="row">
            <div class="wrap">
                <div style="opacity:1 !important;">
                    <center>
                        <div class="login-box">
                            <h2>Recuperar contraseña</h2>
                            <div class="login-logo">
                                <b>Divorcio </b>En Linea
                            </div>
                            <div class="row">
                                <?= $this->Flash->render('auth') ?>
                                <?= $this->Flash->render() ?>
                            </div>
                            <br>
                            <?= $this->Form->create(null, ['action' => 'resetPassword']) ?>
                            <div class="login-box-body">

                                <div class="form-group2 has-feedback">
                                    <?= $this->Form->input('email', [
                                        'class' => 'form-control2 input',
                                        'placeholder' => 'correo eléctronico',
                                        'label' => false,
                                        'required',
                                        'style' => 'margin-left:-1%;'
                                    ]) ?>
                                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                                </div>

                                <?= $this->Form->end() ?>
                                <div class="input-group">
                                    <?= $this->Form->button('Enviar', [
                                        'class' => 'btn btn-primary btn-block btn-flat',
                                        'Controller' => 'Users', 'action' => 'NewContraseña',
                                    ]) ?>
                                </div>
                                <?= $this->Form->end() ?>
                                <br>
                            </div>
                        </div>
                    </center>
                </div>
            </div>
        </div>
    </div>
</section>