<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
 <style>
     @font-face {
        font-family: 'Avenir';
        src: url('../fonts/Avenir-Medium.eot');
        src: local('Avenir Medium'), local('Avenir-Medium'),
            url('../fonts/Avenir-Medium.eot?#iefix') format('embedded-opentype'),
            url('../fonts/Avenir-Medium.woff2') format('woff2'),
            url('../fonts/Avenir-Medium.woff') format('woff'),
            url('../fonts/Avenir-Medium.ttf') format('truetype');
        font-weight: 500;
        font-style: normal;
    }

    ul{
        font-size: 15px;
    }
    h4{
        font-size: 18px;
        font-weight: bold;
    }
    .row{
        font-size: 12px;
        align-content: center;
    }
    li{
        border-bottom: 0px !important;
        border-top: 0px !important;
        border: 0px !important;
        float: none!important;
    }
   
 </style>
 
<!-- Page Content -->
<header class="masthead bg-primary text-white text-center">
	<img src="/img/header-landing.png" alt="" class="ilustracion5">
</header>
<div class="row">
    <div class="col-md-8 col-md-push-2">
            <li class="list-group-item" id="1">
                <h4 style="font-family: avenir;">6 consejos para preparar una consulta <span style="font-size: 0.7em;"><a href="#0"></a></span></h4>
                <p style="font-family: avenir; font-size:15px !important;">
                    No es novedad si te decimos que el tiempo de la consulta es bastante breve y,
                    lamentablemente, eso no depende de nosotros como pacientes. Lo que sí está en
                    nuestras manos, es usar ese tiempo de la mejor manera posible.
                </p>
                    <li class="list-group-item" style="font-family: avenir; font-size:15px !important;"> 
                        Lleva una lista de todos los medicamentos que tomas y sus dosis
                        correspondientes (incluye los que compras sin receta, hierbas y/o vitaminas).
                    </li>
                    <li class="list-group-item" style="font-family: avenir; font-size:15px !important;"> 
                        Reporta tus síntomas y molestias de una forma clara y concisa (puedes seguir
                        los consejos sobre el monitoreo de síntomas para esto).
                    </li>
                    <li class="list-group-item" style="font-family: avenir; font-size:15px !important;"> 
                        Habla de lo que más te preocupa acerca de esos síntomas y sobre lo que
                        piensas que los está causando.
                    </li>
                    <li  class="list-group-item" style="font-family: avenir; font-size:15px !important;">
                        Si estás usando un nuevo medicamento o tratamiento, prepara un breve
                        reporte de cómo te ha ido y cuáles han sido los efectos.
                    </li>
                    <li  class="list-group-item" style="font-family: avenir; font-size:15px !important;">
                        Comparte tus emociones, sentimientos y temores de la manera más clara
                        posible. 
                    </li>
                    <li  class="list-group-item" style="font-family: avenir; font-size:15px !important;">
                        Pregunta para confirmar lo que entendiste sobre el diagnóstico, exámenes,
                        tratamientos y seguimiento.
                    </li>
                    <p style="font-family: avenir; font-size:15px !important; padding: 10px;">
                        Recuerda que necesitas tener la información necesaria para poder tomar decisiones
                        que tienen que ver con tu salud. Si crees que necesitas apoyo, invita a alguien de tu
                        confianza para ir a la consulta contigo, así puede registrar lo que conversen o
                        acuerden con el profesional de la salud.
                    </p>
                </p>
            </li>
        </ul>
    </div>
</div>

<div class="col">
<center>
    <h2 style="font-size:25px;  margin-right:3%; margin-left:3%; font-family: avenir;" >
        Evalúa el estado de tu enfermedad hoy para que puedas compartirlo con tu doctor.
    </h2>
    <br>
    <a href="/list_form"><button type="button" class="btn" style="width:120px; height:40px; font-size:15px; color: white;background-color:#d3562e;">Aquí</button></a>
</center>
</div>
