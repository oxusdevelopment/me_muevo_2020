<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Home
        <small>Preview page</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-3 col-md-push-9 special-box">
            <div class="box box-purple">
                <div class="box-header with-border">
                    <h3 class="box-title">Navegación</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="btn-toolbar">
                                <?= $this->Html->link(
                                    __('<i class="fa fa-repeat fa-side"></i> Volver'),
                                    ['action' => '/index'],
                                    ['class' => 'btn btn-default option', 'escape' => false]
                                ) ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-9 col-md-pull-3 special-box">
            <div class="box box-purple">
                <div class="box-header with-border">
                    <h3 class="box-title">Listado</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th>Título</th>
                                            <th>Cuerpo</th>
                                            <th>Fecha</th>
                                            <th>Activa</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($files as $file) : ?>
                                            <tr>
                                                <td><?= $file->Titulo ?></td>
                                                <td><?= $file->Cuerpo ?></td>
                                                <td><?= $file->Created ?></td>

                                                <td><?= $file->Estado_Contenido == 'A' ? '<i class=" fa fa-check"></i>' : "" ?></td>

                                                <td>
                                                    
                                                    <?= $this->Html->link('<i class="fa fa-edit"></i>', [
                                                        'action' => 'editContenido', $file->ID_Contenido
                                                    ], [
                                                        'escape' => false, 'title' => 'Editar',
                                                        'class' => 'btn btn-box-tool'
                                                    ]);
                                                    ?>
                                                </td>

                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <!--?= $this->element('Admin/Utils/paginator'); ?-->
                </div>
            </div>
        </div>
    </div>

</section>