<?php
namespace App\Controller;

use App\Controller\AppController;

class UsuarioController extends AppController
{
    public function add()
    {
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->Rol = '2';
            $user->Email = $user->Nombre_Usuario;
            if ($this->Users->save($user)) {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    $this->Flash->success(__('Te logeaste correctamente'));
                    // return $this->redirect($this->Auth->redirectUrl());
                }
                $this->Flash->success(__('El usuario se guardó exitosamente.'));
                return $this->redirect(['controller' => 'HomePrivate', 'action' => 'index']);
            }else{
                $this->Flash->error(__('El usuario no pudo guardarse, por favor, intente nuevamente..'));
                return $this->redirect(['controller' => 'Usuario', 'action' => 'add']);
            }
            
        }
        $this->set(compact('user'));
    }

}

