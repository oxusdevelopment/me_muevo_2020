<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ActivityLimitationsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ActivityLimitationsTable Test Case
 */
class ActivityLimitationsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ActivityLimitationsTable
     */
    public $ActivityLimitations;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ActivityLimitations'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ActivityLimitations') ? [] : ['className' => ActivityLimitationsTable::class];
        $this->ActivityLimitations = TableRegistry::getTableLocator()->get('ActivityLimitations', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ActivityLimitations);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
