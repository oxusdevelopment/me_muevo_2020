<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * CommunicationPhysician Entity
 *
 * @property int $id
 * @property int|null $medial_question
 * @property int|null $make_question
 * @property int|null $personal_problem
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 */
class CommunicationPhysician extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
    ];
}
