

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <br>
            <h3 class="title">Capacidad funcional</h3>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-10 col-md-push-1" >
                        <?= $this->Form->create(); ?>
                            <table class="table table-header-rotated" style="font-size: 12px;">
                                <thead>
                                    <tr  class="success">
                                        <th><strong>¿Actualmente puede Ud:</strong></th>
                                        <th class="rotate"><div><strong>Sin ninguna dificultad</strong></div></th>
                                        <th class="rotate"><div><strong>Con alguna dificultad</strong></div></th>
                                        <th class="rotate"><div><strong>Con mucha dificultad</strong></div></th>
                                        <th class="rotate"><div><strong>No puedo hacerlo</strong></div></th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <p><strong>Por favor marque la respuesta que mejor describa sus habilidades usuales (comunes) durante la semana pasada.</strong></p>
                                    <tr>
                                        <th class="row-header"><strong>1.- Vestirse, incluyendo amarrarse los zapatos y abrocharse(abotonarse)?</strong></th>
                                        <td><input type="radio" name="dress" value="3" required></td>
                                        <td><input type="radio" name="dress" value="2" required></td>
                                        <td><input type="radio" name="dress" value="1" required></td>
                                        <td><input type="radio" name="dress" value="0" required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>2.- Acostarse y levantarse de la cama?</strong></th>
                                        <td><input type="radio" name="get_up" value="3" required></td>
                                        <td><input type="radio" name="get_up" value="2" required></td>
                                        <td><input type="radio" name="get_up" value="1" required></td>
                                        <td><input type="radio" name="get_up" value="0" required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>3.-Levantar hasta su boca una taza o vaso lleno?</strong></th>
                                        <td><input type="radio" name="lifting_cup" value="3" required></td>
                                        <td><input type="radio" name="lifting_cup" value="2" required></td>
                                        <td><input type="radio" name="lifting_cup" value="1" required></td>
                                        <td><input type="radio" name="lifting_cup" value="0" required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>4.- Caminar al aire libre en terreno plano?</strong></th>
                                        <td><input type="radio" name="walking" value="3" required></td>
                                        <td><input type="radio" name="walking" value="2" required></td>
                                        <td><input type="radio" name="walking" value="1" required></td>
                                        <td><input type="radio" name="walking" value="0" required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>5.- Bañarse y secarse todo el cuerpo?</strong></th>
                                        <td><input type="radio" name="swim" value="3" required></td>
                                        <td><input type="radio" name="swim" value="2" required></td>
                                        <td><input type="radio" name="swim" value="1" required></td>
                                        <td><input type="radio" name="swim" value="0" required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>6.- Agacharse para recoger ropa del piso?</strong></th>
                                        <td><input type="radio" name="lifting_clothing" value="3" required></td>
                                        <td><input type="radio" name="lifting_clothing" value="2" required></td>
                                        <td><input type="radio" name="lifting_clothing" value="1" required></td>
                                        <td><input type="radio" name="lifting_clothing" value="0" required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>7.- Abrir y cerrar las llaves de agua (los grifos)?</strong></th>
                                        <td><input type="radio" name="open_tap" value="3" required></td>
                                        <td><input type="radio" name="open_tap" value="2" required></td>
                                        <td><input type="radio" name="open_tap" value="1" required></td>
                                        <td><input type="radio" name="open_tap" value="0" required></td>
                                    </tr>
                                    <tr>
                                        <th class="row-header"><strong>8.- Subir y bajar del auto (carro)?</strong></th>
                                        <td><input type="radio" name="get_up_car" value="3" required></td>
                                        <td><input type="radio" name="get_up_car" value="2" required></td>
                                        <td><input type="radio" name="get_up_car" value="1" required></td>
                                        <td><input type="radio" name="get_up_car" value="0" required></td>
                                    </tr>
                                </tbody>
                            </table>
                            <?= $this->Html->link('Volver',
                                ['controller' => 'Pages', 'action' => 'list_form'],
                                ['class' => 'btn btn-danger option', 'escape' => false]) 
                            ?>
                            <?= $this->Form->button('Guardar', ['class' => 'btn btn-md btn-danger option','id' => 'save'],['escape'=>false]); ?>
                            <br>
                        <?= $this->Form->end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div>
