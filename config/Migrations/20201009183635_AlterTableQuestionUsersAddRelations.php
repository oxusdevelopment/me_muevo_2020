<?php
use Migrations\AbstractMigration;

class AlterTableQuestionUsersAddRelations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('question_users');
        $table->addForeignKey('disability_scale_id','disability_scales','id',['delete'=>'CASCADE','update'=>'NO_ACTION']);
        $table->addForeignKey('activity_limitation_id','activity_limitations','id',['delete'=>'CASCADE','update'=>'NO_ACTION']);
        $table->addForeignKey('self_rated_health_id','self_rated_health','id',['delete'=>'CASCADE','update'=>'NO_ACTION']);
        $table->addForeignKey('communication_physician_id','communication_physicians','id',['delete'=>'CASCADE','update'=>'NO_ACTION']);
        $table->addForeignKey('arthritis_selft_efficacy_id','arthritis_selft_efficacies','id',['delete'=>'CASCADE','update'=>'NO_ACTION']);
        $table->update();

    }
}
