<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DisabilityScale Entity
 *
 * @property int $id
 * @property int|null $dress
 * @property int|null $get_up
 * @property int|null $lifting_cup
 * @property int|null $walking
 * @property int|null $swim
 * @property int|null $lifting_clothing
 * @property int|null $open_tap
 * @property int|null $get_up_car
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $created
 * @property int|null $user_id
 */
class DisabilityScale extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dress' => true,
        'get_up' => true,
        'lifting_cup' => true,
        'walking' => true,
        'swim' => true,
        'lifting_clothing' => true,
        'open_tap' => true,
        'get_up_car' => true,
        'modified' => true,
        'created' => true,
        'user_id' => true
    ];
}
