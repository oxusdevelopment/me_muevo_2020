<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
  <li class="treeview">
    <a href="/">
      <i class="fa fa-dashboard"></i> <span>Dashboard </span>
      <span class="pull-right-container">        
      </span>
    </a>
    
  </li>
  <li class="treeview">
    <a href="/admin/Procesos">
      <i class="fa fa-files-o"></i>
      <span>Proceso Mutuo Acuerdo</span>
      <span class="pull-right-container">        
      </span>
    </a>
   
  </li>
  <li>
    <a href="<?php echo $this->Url->build('/pages/widgets'); ?>">
      <i class="fa fa-th"></i> <span>Beneficios del Divorcio</span>
      
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-pie-chart"></i>
      <span>Preguntas Frecuentes</span>
    </a>
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-laptop"></i>
      <span>Noticias</span>
      
    </a>
    
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-edit"></i> <span>Quienes Somos</span>
      
    </a>
   
  </li>
  <li class="treeview">
    <a href="#">
      <i class="fa fa-table"></i> <span>Contacto</span>
    </a>
  </li>
  <li>
    <a href="<?php echo $this->Url->build('/pages/calendar'); ?>">
      <i class="fa fa-user"></i> <span>Usuarios</span>
      
    </a>
  </li>

   <!--li class="header">LABELS</li>
  <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Important</span></a></li-->
 
</ul>