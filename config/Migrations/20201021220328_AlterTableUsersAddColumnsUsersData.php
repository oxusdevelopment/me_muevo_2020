<?php
use Migrations\AbstractMigration;

class AlterTableUsersAddColumnsUsersData extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');

        $table->addColumn('rut', 'string',[
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('birth_date', 'string',[
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('sex', 'string',[
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('healt', 'integer',[
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('phone','integer',[
            'default' => null,
            'null' => true
        ]);
        $table->addColumn('detection', 'string',[
            'default' => null,
            'null' => true
        ]);
        $table->update();
    }
}
