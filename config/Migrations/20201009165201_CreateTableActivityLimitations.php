<?php
use Migrations\AbstractMigration;

class CreateTableActivityLimitations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('activity_limitations');

        $table->addColumn('normal_activity', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('recreational_activity', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('housework', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('errand', 'integer', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'limit' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
