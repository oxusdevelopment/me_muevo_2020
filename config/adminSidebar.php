<?php
$config = [
    'Sidebar' => [
        /*[
            'name' => 'Default', # nombre
            'ico' => false, # ícono (se obtiene desde font-awesome)
            'current' => false, #Si es que tiene un submenu
            'current' => [ # current se usa para agregar la clase "active" a la pestaña escogida y la url respectiva
                'controller' => false, # controller actual
                'action' => false # action del controller actual
            ]
            'submenu' => false, # si no tiene submenú dejar en false
            'submenu' => [
                [
                    'name' => '', # nombre a desplegar en submenú,
                    'current' => false, #Si es que no tiene submenu
                    'current' => [ # current se usa para la clase active
                        'action' => '', # vista de submenú
                        'controller' => '', # controller de submenú
                    ],
                    'submenu' => [
                        'name' => '', # nombre a desplegar en submenú,
                        'current' => [ # current se usa para la clase active
                            'action' => '', # vista de submenú
                            'controller' => '', # controller de submenú
                        ],
                        ...
                    ],
                    ...
                ],
                ...
            ]
        ],*/
        [
            'name' => 'Líneas',
            'ico' => 'fa-dashboard',
            'current' => false,
            'submenu' => [
                [
                    'name' => 'Planificación Anual',
                    'current' => ['action' => 'index','controller' => 'AnnualPlannings']
                ],
                [
                    'name' => 'Planificación Semanal',
                    'current' => ['action' => 'index','controller' => 'MonthlyPlannings']
                ],
                [
                    'name' => 'Servicios',
                    'current' => ['action' => 'index','controller' => 'Routes']
                ],
                [
                    'name' => 'Booking',
                    'current' => false,
                    'submenu' => [
                        [
                            'name' => 'Booking planificado',
                            'current' => ['controller' => 'Bookings', 'action' => 'addPlannedRequest']
                        ],
                        [
                            'name' => 'Booking no planificado',
                            'current' => ['controller' => 'Bookings', 'action' => 'addUnplannedRequest']
                        ],
                        [
                            'name' => 'Mis bookings por aceptar',
                            'current' => ['controller' => 'Bookings', 'action' => 'requested']
                        ],
                        [
                            'name' => 'Bookings declarados',
                            'current' => ['controller' => 'Bookings', 'action' => 'declaration']
                        ],
                        [
                            'name' => 'Bookings solicitados',
                            'current' => ['controller' => 'BookingItineraries', 'action' => 'index']
                        ],
                        [
                            'name' => 'Bookings por confirmar',
                            'current' => ['controller' => 'BookingItineraries', 'action' => 'bookingRequested']
                        ],
                        [
                            'name' => 'Bookings confirmados',
                            'current' => ['controller' => 'BookingItineraries', 'action' => 'confirmed']
                        ],
                        [
                            'name' => 'Solicitudes de modificación',
                            'current' => ['controller' => 'BookingModificationRequests', 'action' => 'index']
                        ], 
                        [
                            'name' => 'Bookings embarcados',
                            'current' => ['controller' => 'BookingItineraries', 'action' => 'embarked']
                        ],
                        [
                            'name' => 'Late arrivals',
                            'current' => ['controller' => 'LateArrivals', 'action' => 'index']
                        ],
                        [
                            'name' => 'Bookings cancelados',
                            'current' => ['controller' => 'BookingItineraries', 'action' => 'canceled']
                        ]
                    ]
                ],
                [
                    'name' => 'General',
                    'current' => ['action' => 'general','controller' => 'BookingItineraries']
                ],
                [
                    'name' => 'Tarifas',
                    'current' => false,
                    'submenu' => [
                        [
                            'name' => 'Acuerdos Líneas',
                            'current' => ['action' => 'index','controller' => 'TariffLines']
                        ],
                        [
                            'name' => 'Acuerdos Clientes Base',
                            'current' => ['action' => 'baseAgreements','controller' => 'TariffClients']
                        ],
                        [
                            'name' => 'Acuerdos Clientes',
                            'current' => ['action' => 'clientsAgreements','controller' => 'TariffClients']
                        ],
                        [
                            'name' => 'Tarifas Líneas',
                            'current' => ['action' => 'listTariffLines','controller' => 'TariffLines']
                        ],
                        [
                            'name' => 'Descuentos Clientes',
                            'current' => ['action' => 'listTariffClients','controller' => 'TariffClients']
                        ],
                        [
                            'name' => 'Reporte Tarifa Embarcados',
                            'current' => ['action' => 'reportEmbarkeds','controller' => 'TariffClients']
                        ],
                    ]
                ],
                [
                    'name' => 'Itinerarios',
                    'current' => false,
                    'submenu' => [
                        [
                            'name' => 'Itinerarios',
                            'current' => ['action' => 'index','controller' => 'Itineraries']
                        ],
                        [
                            'name' => 'Buscar itinerarios',
                            'current' => ['action' => 'searchItineraries','controller' => 'Itineraries']
                        ]
                    ]
                ],
                [
                    'name' => 'Tracking',
                    'current' => ['action' => 'index','controller' => 'Trackings']
                ],
            ]
        ],
    ]
];