<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

/**
 * User mailer.
 */
class UserMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'User';

    public function welcome($user)
    {
    	$name=$user->Nombre.' '.$user->Apellido;

    	$this->to($user->Email)
    		 ->profile('restarPass')
    		 ->emailFormat('html')
    		 ->template('restartPassword')
    		 ->layout('default')
    		 ->viewVars(['name'=>$name,'email'=>$user->email_token])
    		 ->subject(sprintf('Reestablecer tu contraseña ',$name));
    }
}
