<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ArthritisSelftEfficacy Entity
 *
 * @property int $id
 * @property int|null $reduce_pain
 * @property int|null $sleep_pain
 * @property int|null $pain_interfere
 * @property int|null $fequency_activity
 * @property int|null $fatigue
 * @property int|null $help_yourselft_sad
 * @property int|null $daily_pain
 * @property int|null $frustrations
 * @property \Cake\I18n\FrozenTime $modified
 * @property \Cake\I18n\FrozenTime $created
 */
class ArthritisSelftEfficacy extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => true,
    ];
}
