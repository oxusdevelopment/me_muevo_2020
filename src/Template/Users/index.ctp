<!-- Content Header (Page header) -->
<section class="content-header">
      <h1>
      Usuarios
        <small>Preview page</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Usuarios</li>
      </ol>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-3 col-md-push-9 special-box">
        <div class="box box-purple">
            <div class="box-header with-border">
                <h3 class="box-title">Navegación</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                
            <div class="row">
                    <div class="col-md-12">
                        <div class="btn-toolbar">
                            <?= $this->Html->link(__('<i class="glyphicon glyphicon-pencil icon-min"></i> Nuevo Usuario'), 
                                ['action' => 'add'], ['class' => 'btn btn-default option','escape' => false]) 
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>

        </div>

        <div class="col-md-9 col-md-pull-3 special-box">
            <div class="box box-purple">
                <div class="box-header with-border">
                    <h3 class="box-title">Listado</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus icon-special"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover text-center">
                                    <thead>
                                        <tr>
                                            <th>RUT</th>
                                            <th>Nombre de usuario</th>
                                            <th>Nombre</th>
                                            <th>Apellido</th>
                                            <th>Telefono</th>
                                            <th>Acciones</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php foreach ($users as $file) : ?>
                                            <td>
                                                <tr>
                                                    <td><?= $file->RUT ?></td>
                                                    <td><?= $file->Nombre_Usuario ?></td>
                                                    <td><?= $file->Nombre ?></td>
                                                    <td><?= $file->Apellido ?></td>
                                                    <td><?= $file->Telefono ?></td>
                                                    <td class="actions">
                                                        <?= $this->Html->link(__('View'), ['action' => 'view', $file->ID_Usuario]) ?>
                                                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $file->ID_Usuario]) ?>
                                                        <?= $this->Form->postLink(
                                                            __('Delete'),
                                                            ['action' => 'delete', $file->ID_Usuario],
                                                            ['confirm' => __('Are you sure you want to delete # {0}?', $file->ID_Usuario)]
                                                        ) ?>
                                                    </td>


                                                </tr>
                                            </td>
                                            <td>

                                            </td>
                                        <?php endforeach; ?>

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <!--?= $this->element('Admin/Utils/paginator'); ?-->
                    </div>
                </div>
            </div>
        </div>
</section>