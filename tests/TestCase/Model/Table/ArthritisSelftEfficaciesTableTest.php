<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ArthritisSelftEfficaciesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ArthritisSelftEfficaciesTable Test Case
 */
class ArthritisSelftEfficaciesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ArthritisSelftEfficaciesTable
     */
    public $ArthritisSelftEfficacies;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ArthritisSelftEfficacies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ArthritisSelftEfficacies') ? [] : ['className' => ArthritisSelftEfficaciesTable::class];
        $this->ArthritisSelftEfficacies = TableRegistry::getTableLocator()->get('ArthritisSelftEfficacies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ArthritisSelftEfficacies);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
